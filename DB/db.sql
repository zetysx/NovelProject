USE [master]
GO
/****** Object:  Database [novelProject]    Script Date: 7/6/2018 11:59:20 PM ******/
CREATE DATABASE [novelProject]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'novelProject', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\novelProject.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'novelProject_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\novelProject_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [novelProject] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [novelProject].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [novelProject] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [novelProject] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [novelProject] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [novelProject] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [novelProject] SET ARITHABORT OFF 
GO
ALTER DATABASE [novelProject] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [novelProject] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [novelProject] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [novelProject] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [novelProject] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [novelProject] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [novelProject] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [novelProject] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [novelProject] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [novelProject] SET  DISABLE_BROKER 
GO
ALTER DATABASE [novelProject] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [novelProject] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [novelProject] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [novelProject] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [novelProject] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [novelProject] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [novelProject] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [novelProject] SET RECOVERY FULL 
GO
ALTER DATABASE [novelProject] SET  MULTI_USER 
GO
ALTER DATABASE [novelProject] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [novelProject] SET DB_CHAINING OFF 
GO
ALTER DATABASE [novelProject] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [novelProject] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [novelProject] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'novelProject', N'ON'
GO
USE [novelProject]
GO
/****** Object:  Table [dbo].[Chapter]    Script Date: 7/6/2018 11:59:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chapter](
	[chapter_id] [int] IDENTITY(1,1) NOT NULL,
	[novel_id] [int] NOT NULL,
	[chapter_title] [nvarchar](100) NOT NULL,
	[chapter_content] [nvarchar](max) NOT NULL,
	[release_date] [date] NOT NULL,
 CONSTRAINT [PK_Chapter] PRIMARY KEY CLUSTERED 
(
	[chapter_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Favorite]    Script Date: 7/6/2018 11:59:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Favorite](
	[favorite_id] [int] IDENTITY(1,1) NOT NULL,
	[novel_id] [int] NOT NULL,
	[username] [varchar](30) NOT NULL,
 CONSTRAINT [PK_Favorite] PRIMARY KEY CLUSTERED 
(
	[favorite_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 7/6/2018 11:59:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Genre](
	[genre_id] [int] IDENTITY(1,1) NOT NULL,
	[genre] [varchar](30) NOT NULL,
 CONSTRAINT [PK_Genre] PRIMARY KEY CLUSTERED 
(
	[genre_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GenreNovel]    Script Date: 7/6/2018 11:59:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GenreNovel](
	[novel_id] [int] NOT NULL,
	[genre_id] [int] NOT NULL,
 CONSTRAINT [PK_GenreNovel_1] PRIMARY KEY CLUSTERED 
(
	[novel_id] ASC,
	[genre_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Novel]    Script Date: 7/6/2018 11:59:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Novel](
	[novel_id] [int] IDENTITY(1,1) NOT NULL,
	[novel_name] [nvarchar](50) NOT NULL,
	[cover] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
	[author] [nvarchar](50) NOT NULL,
	[year] [int] NULL,
	[status_translate] [bit] NULL,
	[ratings] [float] NULL,
	[views] [int] NULL,
	[last_updated] [datetime] NULL,
	[active] [bit] NOT NULL,
 CONSTRAINT [PK_Novel] PRIMARY KEY CLUSTERED 
(
	[novel_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Review]    Script Date: 7/6/2018 11:59:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Review](
	[review_id] [int] IDENTITY(1,1) NOT NULL,
	[novel_id] [int] NOT NULL,
	[username] [varchar](30) NOT NULL,
	[rating] [int] NOT NULL,
	[review_comment] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Review] PRIMARY KEY CLUSTERED 
(
	[review_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 7/6/2018 11:59:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[username] [varchar](30) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[token] [varchar](50) NULL,
	[fullname] [nvarchar](50) NULL,
	[sex] [varchar](20) NULL,
	[birthday] [date] NULL,
	[avatar] [nvarchar](50) NULL,
	[role] [varchar](20) NOT NULL,
	[enabled] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Chapter] ON 

INSERT [dbo].[Chapter] ([chapter_id], [novel_id], [chapter_title], [chapter_content], [release_date]) VALUES (2, 6, N'Chapter 1: From today onwards, I am a Royal Prince
', N'Chapter 1: From today onwards, I am a Royal Prince

Cheng Yan could sense that someone was calling him.

“Your Highness, please wake up…”

He turned his head away, but the sounds he’d heard didn’t disappear, they actually proceeded to get even louder instead. Then, he felt someone gently tug on his sleeve.

“Your Highness, my Royal Prince!”

Cheng Yan’s eyes snapped open. His familiar surroundings had disappeared, his work desk was gone, and the familiar walls filled with post-its were gone. They’d all been replaced by a strange landscape. A round public square that was enclosed by small brick houses, and the gallows that were erected in the center of the square now dominated his field of view. He himself sat at a table across the square from the gallows. There wasn’t a soft rotating office chair under his butt, but a cold hard iron chair instead. There was also a group of people sitting with him and watching him intently. Several of them were dressed as medieval lords and ladies from those Western flicks, and were trying to suppress their giggles.

What the hell? Wasn’t I just rushing to finish my mechanical blueprints before the deadline? Cheng Yan was at a loss as he thought to himself. For three consecutive days, he had been working overtime. Thus, he was both mentally and physically at his limit. He could only vaguely remember that his heartbeat had become unsteady, and that he’d just wanted to lie down on his desk and take a break…

“Your Highness, please declare your ruling.”

The speaker was the one that had secretly tugged on his sleeve. His face was old, seemingly in his fifties or sixties, and he wore a white robe. At first glance, he looked a bit like Gandalf, from The Lord of the Rings.

Am I dreaming? Cheng Yan thought as he licked his dry lips, Ruling? What ruling?

As he quickly glanced around, his confusion was swept away. The people surrounding him were all looking in the direction of the center of the square, at the gallows. Many townspeople were also in the plaza and were waving their fists while they shouted and even threw an occasional stone towards the gallows and the figure on it.

Cheng Yan had only ever seen such an ancient instrument of death in movies. The gallows consisted of two pillars extending upwards about 4 meters from a raised base, with a crossbeam extending between the two pillars with a thick yellow hemp rope around the middle of the crossbeam. One end of the rope was tied to the gallows, and the other end was tied into a noose around a prisoner’s neck.

In this strange dream Cheng Yan thought he was in, he found that he was able to see everything clearly. Usually, he’d even need to wear his glasses to see the words on a computer screen, but now Chen Yang could see every detail of the gallows, which were fifty meters away, without his glasses.

The prisoner atop the gallows had their head completely covered with a hood and had their hands tied behind their back. They wore dirty grey clothes that were little more than rags draped over a frame so thin, it seemed you could easily wrap your hand around their exposed ankle. Cheng Yan judged the prisoner to be female by her faintly bulging chest, and looked on as she stood there shivering in the chilly wind, but still trying to stand up straight to face her fate on her feet.

Alright then, Cheng Yan thought to himself, what crime did this woman commit that caused so many people to be so outraged, and to wait for her to be hanged with such rage and hostility?

Cheng Yan’s memories appeared, almost as if they’d suddenly been turned on and he realized the cause of the situation, and the answer to his question, at almost the same time.

She was a “witch”.

She was considered to have fallen to the temptation of the devil and was known as an incarnation of evil.

“Your Highness?” The Gandalf lookalike cautiously urged.

Cheng Yan glanced at the old man. Well, Cheng Yan’s new memories told him, the old man wasn’t called Gandalf, his real name was Barov, and he was an Assistant Minister of Finance dispatched by the Roland’s father to assist in the governing of the territory.

Cheng Yan’s identity was that of the 4th Prince of the Kingdom of Graycastle, Roland, and he had been sent here to govern this region. The residents of this border town had caught and seized the witch, immediately turning her over to the local guards to question. Questioning? No, She was immediately sent to be sentenced with no opportunity to defend herself. The execution of suspected witches was usually overseen by the local lords or bishops, but since he’d assumed control of this territory, issuing such orders had become his obligation.

Cheng Yan’s memory answered his questions one by one, it was unnecessary to filter and read through them, it was as if they had always been his own experiences. He was momentarily confused, there was absolutely no way a dream could have so many details. Then, Cheng Yan thought, was it possible that this wasn’t a dream? I’ve really traveled through time, to the dark ages of medieval Europe, and have become Roland? I’ve gone from a pitiful mechanical engineer with his nose down in his papers to a grand 4th Prince overnight?

This piece of territory that looked so barren and backward was in the Kingdom of Graycastle, a name that he had never seen in his history books.

Well, then how do I want to handle this? Cheng Yan thought to himself.

Cheng Yan decided he would try and examine how an unscientific thing like being transported through time and space had happened later, his immediate concern was with how to stop the farce taking place in front of him. Assigning the blame for the disasters and misfortune that befell them onto these “witches” was the act of ignorant barbarians. He really couldn’t bring himself to do anything as stupid as hanging another person just to satisfy the watching masses.

He grabbed the formal written orders held by Barov and tossed them to the ground and slowly said, “I’m feeling tired, we will give our judgement another day. Court dismissed, now disperse people!”

Cheng Yan knew he couldn’t risk being reckless, so he rummaged carefully through his memories and reflected the former prince’s behavior. He had to continue on with the former prince’s dandyism and roguish behavior. That’s right, the fourth prince himself was messed up, had a nasty character, and did whatever he wanted with no thoughts to the consequences of his actions. Anyways, Cheng Yan mused, could they really expect an uncontrollable twenty-something year old to have good behavior?

The members of the nobility who sat with him maintained their equanimity at his unexpected statement, but a tall man wearing a suit of armor stood up and argued, “Your Highness, this isn’t a joke! All known witches should be put to death immediately upon being identified, or other witches might be tempted to try and save her! Do you want to force the church to get involved when they hear that we have allowed a witch to live? We have no choice in this matter!”

Carter, this dashing man, was actually his Knight Commander. Cheng Yan frowned and said, “Why? Are you scared?” His voice was full of blatant mockery and wasn’t a complete act. A man with an arm thicker than the waist of the so called “witch” actually feared a prison raid from women. Were witches really the devil’s messengers? “Wouldn’t it be better to catch more witches than to settle for only one?”

Seeing him no longer utter a word, Cheng Yan waved his hand to call his personal guards and left. Carter hesitated a moment before going down and catching up with the troops walking by the 4th prince’s side. The other nobles got up and paid their respects to the prince, but Cheng Yan could see undisguised contempt from the eyes of those in the crowd.

Back in the keep, the castle was located to the south of the border town, he dismissed the anxious Minister Barov outside the door to his chambers, allowing him to finally breathe a sigh of relief now that he was alone.

As a person who’d spent ninety percent of his time dealing with people through a computer, facing everyone like he just had already surpassed his comfort zone. Cheng Yan found the location of his bedroom from his new memories, took a seat on his bed, and got a moment of real rest as he tried to suppress his violently beating heart. At the moment, the most important matter was to clarify the situation. Why was the prince, who couldn’t stay in Wimbledon City, the capital of the kingdom, sent to this barren land?

The unexpected answer he came up with left him stupefied.

Roland Wimbledon was actually sent here to fight for the right to succeed the king.

Everything had originated from King Wimbledon III of Graycastle’s wonderful proclamation to his children saying, “You want to inherit the kingdom? The first-born prince doesn’t necessarily have the right to become king, only the person who proves themselves as the most capable of governing can inherit the country.” He placed various territories under the rule of his five children, and after five years he’d decide who would become his successor based on the level of skill they displayed in governing their respective territories.

While turning the decision of who should inherit the throne into a meritocracy and providing equal opportunity regardless of gender might sound like very enlightened concepts, the real problem was with the actual implementation of said ideas. Would there be any guarantee that all five of them received the same starting conditions? This wasn’t like playing a real-time strategy game. To his knowledge, the second son had been given a better territory than this border town. Actually when he thought about it, it seemed that among the five regions they’d been given, none of the others were worse than his frontier town. His starting point was simply inferior.

Also, Cheng Yan wondered, how was one to assess the level of governance? By the population? Military power? Economic standing? Wimbledon III hadn’t mentioned any standard, nor did he put the slightest restrictions on their methods of competition. In case someone secretly assassinated the other candidates, what would he do? Would the queen stand by and watch her children kill each other? Wait. …… He carefully recalled the next memory, all right, another piece of bad news; the Queen had died five years ago.

Cheng Yan sighed. Obviously, this was a barbaric and dark feudal era he had found himself in. Just the way they seemed to wantonly kill witches was enough to give him a few hints. Also, Cheng Yan thought, why would he want to become king? With no internet and none of the comforts of modern civilization, he’d have to live the same life as the native people. Burning witches for fun, living in a city where everyone dumped their excrement wherever they wished, and finally dying from the Black Death.

Cheng Yan being a prince could already be considered a very high starting point. Even if he didn’t become king he was still of royal blood and had already been knighted. As long as he managed to stay alive he would be considered as one of the Lords of the Realm.

Cheng Yan suppressed his wandering thoughts and went to his bedroom mirror. The man looking back at him in the mirror had light gray hair, which was the royal family’s most distinctive feature. His face was slightly pale and with his regular facial features, he seemed to be completely without personality traits. He appeared to be lacking in physical exercise and as for wine and woman, he recalled indulging in both with some regularity. He had had several lovers in the King’s City, but all had been willing participants, he hadn’t forced anyone.

As for the cause of his own crossing over… Cheng Yan guessed that thanks to the company’s inhuman urging to progress forward, his boss had arranged for him to work overtime, which in turn actually led to the tragedy that was his sudden death. The victims of cases like these were usually coders, mechanical engineers, and programmers.

In the end, no matter what, at least I got the equivalent of an extra life. I really shouldn’t complain too much, in the coming days, I might be able to slowly improve this life, but my first task is to play a convincing 4th Prince, so that other people don’t find something amiss with my behavior and think I’m possessed by the devil, leading to my being burned at the stake, Cheng Yan thought to himself.

“So, in order to live well…” Cheng Yan took a deep breath, looked in the mirror, and whispered, “from now on, I’m Roland.”
', CAST(N'2012-08-08' AS Date))
INSERT [dbo].[Chapter] ([chapter_id], [novel_id], [chapter_title], [chapter_content], [release_date]) VALUES (3, 6, N'Chapter 2 The Witch Named Anna (Part I)
', N'Chapter 2 The Witch Named Anna (Part I)

For a period of time Roland locked himself in his room as he carefully reviewed the memories of this new world, such that dinner had to be sent directly to him by his servants.

Roland suppressed his fear of the unfamiliar environment he found himself in under his strong will to live. He was very clear that if he wanted to blend in and avoid being suspected by the people around him he needed to get more information as soon as possible.

Roland had to say that the fourth prince had, apart from fooling around with some other sons of the nobility, no additional things in his brain. Over and over again, Roland was unable to remember any valuable information such as knowledge of the aristocracy, the political situation in his own country, or the diplomatic situation with his neighbors. As for basic common sense, such as city names, or the years of significant events, they were completely different than the history of Europe he knew.

It seemed that based on his memories, the old Roland had had absolutely no chance of obtaining the throne. Perhaps the King of Graycastle was aware of this, and because of that, the prince had been thrown into this hellish place, even if he made a mess of things in this border town, it wouldn’t result in much damage to the kingdom.

The next memories Roland looked at were of his brothers and sisters, and what he found left him unsure whether he should laugh or cry.

Roland’s eldest brother, the First Prince, had an above average military power, his second brother was scheming and horridly treacherous, his third sister was afraid of death, and his younger sister was brilliant. This was the entirety of the former fourth prince’s impressions of his siblings. Roland felt a little awkward, after more than a decade of living with them the old Roland’s knowledge had been summed up in a few words. What forces they’d developed, who their competent subordinates were, what they were experts at, what their plans were and so on…he knew nothing at all.

It was only three months ago that the fourth prince had come to this frontier town, but the nobility had already stopped hiding their contempt for him. It was obvious that the fourth prince wasn’t cut out to be a leader. Fortunately, when the King had left Roland this territory, he had sent along two of his more capable subordinates to provide assistance so the townspeople wouldn’t suffer under the old Roland’s inept rule.

After Roland woke up the next morning one of his maids, Tyre, repeatedly mentioned that the Assistant Minister wanted to see him. When it seemed that he could put it off no longer Roland acted according to his past memories and reached out to cup the maid’s ass before sending her to fetch Barov, who had been waiting in the drawing room.

Seeing the flushing Tyre exit the room, Roland suddenly realized that, since he had reincarnated, shouldn’t he have a system or something like that? At least in many tales that was the standard formula, but the arrival of a system never happened.

Sure enough, what Roland had read in those novels was all fiction.

****

In the drawing room, Barov was already restless from waiting. The moment Roland appeared he asked, “Your Highness, why didn’t you order the execution yesterday?”

“One day earlier, one day later, what’s the difference?” Roland said as he clapped his hands, letting the attendants know to bring his breakfast in, “Sit down, Barov.”

The impressions he had from the old Roland’s memories, and also based on his own opinion, was that the Knight Commander liked to confront problems with the fourth prince directly face to face, even in the presence of others, while the Assistant Minister was more circumspect and liked to discuss issues in private. In any case, the loyalty of the two was likely to be to the King.

“A day later may lead to other witches appearing, my royal prince! This isn’t the same as before with your previous escapades, not during this time of chaos!” Barov cautioned.

“How can you even say that?” Roland asked while frowning, “I thought you were capable of distinguishing the differences between superstition and fact.”

Barov looked bewildered, “What superstitions?”

“That a witch is evil and the devil’s messenger,” Roland seemed to not mind as he patiently answered the question. “Isn’t that what the church teaches us? They won’t intervene here, I think it’s actually the opposite. Their propaganda states that witches are evil, and while we’ve chosen not to actively aid their witch hunt, all the people in this territory believe in these shameless superstitions spread by the Church.”

Barov was shocked, “Could…could a witch really be…”

“Indeed evil?” Roland asked, “Like what?”

The Assistant Minister was silent for a moment, trying to decide if the prince was deliberately making fun of him, “Your Highness, this problem can be discussed later. I know you don’t like the church, but this pursuit of conflict is counterproductive.”

Roland curled his lips. It seemed that reversing this superstition about witches wasn’t something that he could do overnight, but for now he decided to put it out of his mind..

When Roland’s breakfast of toast, fried eggs and a carafe of milk arrived he made up two plates, one of which he served to the assistant minister.

“You haven’t eaten until now, right?” asked Roland before he started eating. The maid had told him that Barov had arrived outside his chambers at dawn, and had directly requested to see him, so he shouldn’t have had time to eat. While he’d decided to imitate the former prince’s way of life, he’d also decided to begin to change the way people perceived him a bit at a time.

The Assistant Minister was a good first target for his plan. Roland thought to himself, If you can make your men feel valued, then they’ll be more motivated to work for you.

Taking the initiative had always been the most efficient way to win, hadn’t it?

Barov took the cup of milk Roland handed him but didn’t drink as he anxiously said, “Your Highness, we still have a problem. The guards reported that three days ago a suspected witch camp was found in the western forest. Because they left in a hurry and didn’t clean up all of their traces, a guard found this in the camp.”

He took out a coin from his pocket and put it in front of Roland. This wasn’t the common currency of the kingdom, at least according to the memories of the old Roland, he hadn’t seen such a coin. It wasn’t even like theirs, it wasn’t even made of metal.

Feeling it in his his hands, he was surprised to find that the coin was warm, and the assistant minister definitely wasn’t the source of this sweltering heat of at least forty degrees celsius, which reminded him of the moment when one took a bath.

“What is this?” Roland asked.

“I thought it was just some foul trinket that a witch made, but it’s actually more serious than that.” Barov had to pause to wipe his forehead, “the printed pattern is known as the Devil’s Eye of the Sacred Mountain, which is the emblem of the Witch Cooperation Association.”

Roland rubbed the coin’s uneven surface, he guessed that it was probably fired ceramic. Indeed, he saw that the center of the coin depicted a “mountain” shaped pattern of three triangles juxtaposed with one eye in the centre triangle. The pattern’s contour lines were very rough, he judged that it should’ve been polished by hand.

Roland recalled the two terms ”Devil’s Eye of the Sacred Mountain” and the “Witch Cooperation Association”, but wasn’t able to discover any details. It seemed that the fourth prince had had no interest in occultism.

Roland didn’t expect that Barov knew more, but he continued, “Your Highness, you haven’t seen real witches before, so it’s understandable if you think their abilities are exaggerated. Indeed, they can be injured, they’ll even bleed and aren’t any harder to kill than the rest of us, but that’s only for a witch who can’t resist. When they receive the devil’s power it can shorten the lifespan of a witch, but it can also give them terrible power. Ordinary people just can’t match them. Once a witch grows to adulthood, even an army will have to pay a high price to kill her. Their desires are almost impossible to suppress, ultimately causing them to degenerate into the devil’s minions.“The Church therefore declared a Holy Inquisition, If a woman is found to have even a chance to be a witch they’re to be immediately seized and executed. The King has also approved of this decree and in fact, these measures have been highly effective and the incidents where witches have wreaked havoc have already greatly declined in comparison to a hundred years ago. The Sacred Mountain, or to say the doorway to hell, is only a rumor illustrated in an ancient book from that era.”

Roland, while gnawing on his bread, sneered again and again as he heard this. Although the histories of this world and the world he knew were very different, their historical trajectories were surprisingly similar. No matter if it was the church in this world or the church he knew from his, he thought that religion itself was the devil’s minion, the real source of evil. You don’t think sentencing someone to death only because they are different isn’evil? Using God’s name to kill someone was all kinds of wrong.Unaware of Roland’s thoughts, Barov continued with his speech, “Recorded in ancient books is that witches can only find real peace at the Sacred Mountain. They wouldn’t have to suffer uncontrollable desires because their magic would have no side effects. There’s no doubt that the so-called Sacred Mountain was certainly the birthplace of evil, an entrance to hell on earth. I think that only hell won’t punish those who’ve fallen for the devil’s temptations.”

“The “League of Allied Witches,” who are they? What’s their relationship with the Sacred Mountain?” Roland asked.

Barov explained with a sour face, “In the past, everything was good because the witches would run away before the Inquisition arrived and were living in seclusion. But in recent years, the League of Allied Witches appeared and made a difference. They want to gather all of the witches and find the Sacred Mountain. For this purpose, the Witch Cooperation Association will even take the initiative of luring others into becoming a witch. In the last year, many babies disappeared in the Port of Clearwater, and the rumor was that it was their doing.”
', CAST(N'2012-09-08' AS Date))
INSERT [dbo].[Chapter] ([chapter_id], [novel_id], [chapter_title], [chapter_content], [release_date]) VALUES (5, 1, N'atelier tanaka', N'atelier tanaka', CAST(N'2013-08-08' AS Date))
SET IDENTITY_INSERT [dbo].[Chapter] OFF
SET IDENTITY_INSERT [dbo].[Genre] ON 

INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (1, N'Action')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (2, N'Fantasy')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (3, N'Horror
')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (4, N'Sci-fi')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (5, N'Supernatural
')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (6, N'Slice of Life')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (7, N'Tragedy
')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (8, N'Adventure
')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (9, N'Comedy
')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (10, N'Martial Arts
')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (11, N'Shoujo
')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (12, N'Drama
')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (13, N'Mystery')
INSERT [dbo].[Genre] ([genre_id], [genre]) VALUES (14, N'Seinen')
SET IDENTITY_INSERT [dbo].[Genre] OFF
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (1, 1)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (1, 2)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (1, 3)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (1, 4)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (1, 5)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (2, 3)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (2, 5)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (3, 1)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (3, 4)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (3, 5)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (3, 6)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (3, 7)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (4, 1)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (4, 2)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (6, 2)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (6, 5)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (6, 13)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (6, 14)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (14, 1)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (14, 2)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (14, 13)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (36, 1)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (36, 2)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (36, 5)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (36, 8)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (36, 9)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (46, 1)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (46, 2)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (46, 5)
INSERT [dbo].[GenreNovel] ([novel_id], [genre_id]) VALUES (46, 6)
SET IDENTITY_INSERT [dbo].[Novel] ON 

INSERT [dbo].[Novel] ([novel_id], [novel_name], [cover], [description], [author], [year], [status_translate], [ratings], [views], [last_updated], [active]) VALUES (1, N'atelier', N'https://i.imgur.com/Y9FLdN2.png', NULL, N'kek', 2015, 0, 3.5, 1, NULL, 1)
INSERT [dbo].[Novel] ([novel_id], [novel_name], [cover], [description], [author], [year], [status_translate], [ratings], [views], [last_updated], [active]) VALUES (2, N'tales of berseria', N'https://i.imgur.com/BbPtbct.jpg', NULL, N'tales', 2014, 0, 5, 1, NULL, 1)
INSERT [dbo].[Novel] ([novel_id], [novel_name], [cover], [description], [author], [year], [status_translate], [ratings], [views], [last_updated], [active]) VALUES (3, N'tales of vesperia', N'https://i.imgur.com/JHzYPho.jpg', NULL, N'tales', 2013, 0, 0, 4, NULL, 1)
INSERT [dbo].[Novel] ([novel_id], [novel_name], [cover], [description], [author], [year], [status_translate], [ratings], [views], [last_updated], [active]) VALUES (4, N'tales of zestiria', N'https://i.imgur.com/NuivTJi.jpg', NULL, N'tales', 2012, 0, 0, 7, CAST(N'2018-01-01 02:30:50.020' AS DateTime), 1)
INSERT [dbo].[Novel] ([novel_id], [novel_name], [cover], [description], [author], [year], [status_translate], [ratings], [views], [last_updated], [active]) VALUES (6, N'Release that Witch
', N'https://i.imgur.com/STPWjLS.jpg', N'Chen Yan travels through time, only to end up becoming an honorable prince in the Middle Ages of Europe. Yet this world was not quite as simple as he thought. Witches with magical powers abound, and fearsome wars between churches and kingdoms rage throughout the land.

Roland, a prince regarded as hopeless by his own father and assigned to the worst fief, spends his time developing a poor and backward town into a strong and modern city, while fighting against his siblings for the throne and absolute control over the kingdom. Join Roland as he befriends and allies with witches and, through fighting and even farming, pushes back invaders coming from the realm of evil.

', N'Er Mu
', 2016, 0, 4, 35, CAST(N'2018-01-01 00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Novel] ([novel_id], [novel_name], [cover], [description], [author], [year], [status_translate], [ratings], [views], [last_updated], [active]) VALUES (14, N'Dungeon Defense', N'https://i.imgur.com/kr02Yhp.jpg', N'Do you know how this world ends? Become the hero and defeat the 72 Demon Lords. The game that was boasted as the absolute hardest strategy game, 『Dungeon Attack』. I used to be the ‘hero’ that had accomplished everything in this game, but after answering a suspicious survey, I found myself in the game as the weakest Demon Lord, 「Dantalian」. With only my eloquent tongue and my memories of conquest as a hero—. In order to survive as Dantalian. I shall tear this world apart.', N'Yoo Heonhwa', 2016, 0, 4, 30, CAST(N'2018-06-07 21:34:31.077' AS DateTime), 1)
INSERT [dbo].[Novel] ([novel_id], [novel_name], [cover], [description], [author], [year], [status_translate], [ratings], [views], [last_updated], [active]) VALUES (36, N'Overlord', N'https://i.imgur.com/fNyCpX7.jpg', N'After announcing it will be discontinuing all service, the internet game ‘Yggdrasil’ shut down… That was the plan, but for some reason, the player character did not log out some time after the server was closed. NPCs start to become sentient. A normal youth who loves gaming in the real world seemed to have been transported into an alternate world along with his guild, becoming the strongest mage with the appearance of a skeleton, Momonga. He leads his guild ‘Ainz Ooal Gown’ towards an unprecedented legendary fantasy adventure!', N'MARUYAMA Kugane', 2010, 0, 4, 20, CAST(N'2018-06-07 23:30:52.797' AS DateTime), 1)
INSERT [dbo].[Novel] ([novel_id], [novel_name], [cover], [description], [author], [year], [status_translate], [ratings], [views], [last_updated], [active]) VALUES (46, N'Atelier Tanaka', N'https://i.imgur.com/nK6doYe.png', N'Yoshio Tanaka gets reincarnated into a fantasy game world, with healing magic as a “cheat” ability.', N'Buncololi', 2014, 0, 3, 2, CAST(N'2018-06-07 23:39:10.093' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Novel] OFF
SET IDENTITY_INSERT [dbo].[Review] ON 

INSERT [dbo].[Review] ([review_id], [novel_id], [username], [rating], [review_comment]) VALUES (10, 1, N'zetysx', 4, N'its ok')
INSERT [dbo].[Review] ([review_id], [novel_id], [username], [rating], [review_comment]) VALUES (11, 1, N'kek', 5, N'its ok')
INSERT [dbo].[Review] ([review_id], [novel_id], [username], [rating], [review_comment]) VALUES (12, 1, N'TriHard', 3, N'its ok')
INSERT [dbo].[Review] ([review_id], [novel_id], [username], [rating], [review_comment]) VALUES (13, 1, N'HotPocket', 2, N'so bald')
INSERT [dbo].[Review] ([review_id], [novel_id], [username], [rating], [review_comment]) VALUES (14, 6, N'zetysx', 4, N'7/10 too much water')
SET IDENTITY_INSERT [dbo].[Review] OFF
INSERT [dbo].[Users] ([username], [password], [token], [fullname], [sex], [birthday], [avatar], [role], [enabled]) VALUES (N'HotPocket', N'456', NULL, NULL, N'female', NULL, NULL, N'ROLE_ADMIN', 1)
INSERT [dbo].[Users] ([username], [password], [token], [fullname], [sex], [birthday], [avatar], [role], [enabled]) VALUES (N'kek', N'123', NULL, NULL, N'male', NULL, NULL, N'ROLE_USER', 1)
INSERT [dbo].[Users] ([username], [password], [token], [fullname], [sex], [birthday], [avatar], [role], [enabled]) VALUES (N'TriHard', N'123', NULL, NULL, N'male', NULL, NULL, N'ROLE_USER', 1)
INSERT [dbo].[Users] ([username], [password], [token], [fullname], [sex], [birthday], [avatar], [role], [enabled]) VALUES (N'zetysx', N'123', NULL, NULL, N'male', NULL, NULL, N'ROLE_ADMIN', 1)
ALTER TABLE [dbo].[Chapter]  WITH CHECK ADD  CONSTRAINT [FK_Chapter_Novel] FOREIGN KEY([novel_id])
REFERENCES [dbo].[Novel] ([novel_id])
GO
ALTER TABLE [dbo].[Chapter] CHECK CONSTRAINT [FK_Chapter_Novel]
GO
ALTER TABLE [dbo].[Favorite]  WITH CHECK ADD  CONSTRAINT [FK_Favorite_Novel] FOREIGN KEY([novel_id])
REFERENCES [dbo].[Novel] ([novel_id])
GO
ALTER TABLE [dbo].[Favorite] CHECK CONSTRAINT [FK_Favorite_Novel]
GO
ALTER TABLE [dbo].[Favorite]  WITH CHECK ADD  CONSTRAINT [FK_Favorite_User] FOREIGN KEY([username])
REFERENCES [dbo].[Users] ([username])
GO
ALTER TABLE [dbo].[Favorite] CHECK CONSTRAINT [FK_Favorite_User]
GO
ALTER TABLE [dbo].[GenreNovel]  WITH CHECK ADD  CONSTRAINT [FK4aky2v4bnyhkloe7r3l74i35j] FOREIGN KEY([genre_id])
REFERENCES [dbo].[Genre] ([genre_id])
GO
ALTER TABLE [dbo].[GenreNovel] CHECK CONSTRAINT [FK4aky2v4bnyhkloe7r3l74i35j]
GO
ALTER TABLE [dbo].[GenreNovel]  WITH CHECK ADD  CONSTRAINT [FKeb9r89wctdtyrqbu7ay46g0mr] FOREIGN KEY([novel_id])
REFERENCES [dbo].[Novel] ([novel_id])
GO
ALTER TABLE [dbo].[GenreNovel] CHECK CONSTRAINT [FKeb9r89wctdtyrqbu7ay46g0mr]
GO
ALTER TABLE [dbo].[Review]  WITH CHECK ADD  CONSTRAINT [FK_Review_Novel] FOREIGN KEY([novel_id])
REFERENCES [dbo].[Novel] ([novel_id])
GO
ALTER TABLE [dbo].[Review] CHECK CONSTRAINT [FK_Review_Novel]
GO
ALTER TABLE [dbo].[Review]  WITH CHECK ADD  CONSTRAINT [FK_Review_User] FOREIGN KEY([username])
REFERENCES [dbo].[Users] ([username])
GO
ALTER TABLE [dbo].[Review] CHECK CONSTRAINT [FK_Review_User]
GO
USE [master]
GO
ALTER DATABASE [novelProject] SET  READ_WRITE 
GO
