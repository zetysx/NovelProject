//Loading
$body = $("body");
$(document).on({
    ajaxStart: function () {
        $body.addClass("loading");
    },
    ajaxStop: function () {
        $body.removeClass("loading");
    }
});
////////////////////////////////////
var table;
var novelId;
var linkAddChapter;
var chapterId;
var chapterEdit;
var index=1;
$(document).ready(function () {
    novelId = $("#novel_id").val();
    linkAddChapter = "/admin/novels/chapter/addChapter/" + novelId;
    $("#addNewChapter").attr("href", linkAddChapter);
    table = $("#listChapterTable").DataTable({
        columnDefs: [{
            searchable: false,
            orderable: true,
            targets: 0
        }],
        order: [[1, 'asc']],
        processing: true,
        ajax: {
            url: "/api/admin/novels/" + novelId + "/chapters",
            dataSrc: function (json) {
                $("#exampleInputNovelName").val(json.contents.novemName);
                $("#exampleInputAuthor").val(json.contents.author);
                return json.contents.chapters;
            }
        },
        columns: [
            {data: processIndex, searchable: false},
            {data: "chapterTitle", searchable: true},
            {data: processDataContent, searchable: false, width: "70%"},
            {data: processDataButton, searchable: false}
        ]
    });
    //Submit edit
    $("#btnEditChapter").click(function () {
        chapterId = $("#chapter_id").val();
        chapterEdit =
            {
                title: $("#exampleInputTitle").val(),
                content: $("#exampleInputContent").val()
            };
        $.ajax({
            contentType: "application/json; charset=utf-8",
            type: 'POST',
            url: "/api/admin/chapters/" + chapterId,
            data: JSON.stringify(chapterEdit),
            success: function (response) {
                //Check error exist
                if ($("#exampleInputErrorTitle").css("display") === "block") {
                    $("#exampleInputErrorTitle").css("display", "none");
                    $("#exampleInputErrorTitle").val("");
                }
                if ($("#exampleInputErrorContent").css("display") === "block") {
                    $("#exampleInputErrorContent").css("display", "none");
                    $("#exampleInputErrorContent").val("");
                }
                $("#exampleEditModal").css("display", "none");
                // Table reload
                table.ajax.reload();
                location.reload();
            },
            error: function (response) {
                //Check error exist
                if ($("#exampleInputErrorTitle").css("display") === "block") {
                    $("#exampleInputErrorTitle").css("display", "none");
                    $("#exampleInputErrorTitle").val("");
                }
                if ($("#exampleInputErrorContent").css("display") === "block") {
                    $("#exampleInputErrorContent").css("display", "none");
                    $("#exampleInputErrorContent").val("");
                }
                //////////////////
                var errorNotification = response.responseJSON.errors;
                for (var i = 0; i < errorNotification.length; i++) {
                    fieldNameError = errorNotification[i].fieldName;
                    if (fieldNameError === "Chapter Titile") {
                        $("#exampleInputErrorTitle").css("display", "block");
                        $("#exampleInputErrorTitle").val(errorNotification[i].fieldError);
                    }
                    if (fieldNameError === "Chapter Content") {
                        $("#exampleInputErrorContent").css("display", "block");
                        $("#exampleInputErrorContent").val(errorNotification[i].fieldError);
                    }
                }
                // console.log(response);
                //move to top
                $("body,html").animate({scrollTop: 0}, "slow");
            }
        });
    });
    //buttong cancel
    $("#exampleCancel").click(function () {
        //Check error exist
        if ($("#exampleInputErrorTitle").css("display") === "block") {
            $("#exampleInputErrorTitle").css("display", "none");
            $("#exampleInputErrorTitle").val("");
        }
        if ($("#exampleInputErrorContent").css("display") === "block") {
            $("#exampleInputErrorContent").css("display", "none");
            $("#exampleInputErrorContent").val("");
        }
    });
    //buttong exit
    $("#exampleExit").click(function () {
        //Check error exist
        if ($("#exampleInputErrorTitle").css("display") === "block") {
            $("#exampleInputErrorTitle").css("display", "none");
            $("#exampleInputErrorTitle").val("");
        }
        if ($("#exampleInputErrorContent").css("display") === "block") {
            $("#exampleInputErrorContent").css("display", "none");
            $("#exampleInputErrorContent").val("");
        }
    });

    function processIndex(data, type, full, meta) {
        return '<span>'+ index++ + '</span>';
    }

    function processDataContent(data, type, full, meta) {
        var contentBefore = data.content;
        var contentAfter = contentBefore;
        if (contentBefore.length > 998) {
            contentAfter = contentAfter.substring(0, 1000) + "......";
        }
        return '<p>' + contentAfter + '</p>';
    }

    function processDataButton(data, type, full, meta) {
        return '<button class="btn btn-danger" data-toggle="modal" onclick="getDetail(' + data.chapterId + ')" id="editChapter" data-target="#exampleEditModal" value=' + data.chapterId + '>Edit</button>';
    }


});

function getDetail(chapterId) {
    $.ajax({
            type: 'GET',
            url: "/api/admin/chapters/" + chapterId,
            headers: {
                Accept: 'application/json'
            },
            success: function (response) {
                $("#chapter_id").val(response.contents.chapterId);
                $("#exampleInputTitle").val(response.contents.chapterTitle);
                $("#exampleInputContent").val(response.contents.chapterContent);
            }
        }
    );
}

