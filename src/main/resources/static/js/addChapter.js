//Loading
$body = $("body");
$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
    ajaxStop: function() { $body.removeClass("loading"); }
});
// add novel
var novelId = $("#novel_id").val();
var titleChapter;
var contentChapter;
$(document).ready(function () {
    $.ajax("/api/admin/novels/" + novelId).done(function (response) {
        console.log(response);
        $("#exampleInputNovelName").val(response.contents.novelName);
        $("#exampleInputAuthor").val(response.contents.author);
    });
});
$("#btnAddChapter").click(function () {
    titleChapter = $("#exampleInputTitle").val();
    contentChapter = $("#exampleInputContent").val();
    //Object
    var newChapter =
        {
            novelID: novelId,
            title: titleChapter,
            content: contentChapter
        };
    var valueCss;
    $.ajax({
        contentType: "application/json; charset=utf-8",
        type: 'POST',
        url: "/api/admin/chapters",
        data: JSON.stringify(newChapter),
        success: function (response) {
            // alert(JSON.stringify(response.message));
            valueCss = $("#exampleInputNotification").css("display");
            if (valueCss === "block") {
                $("#exampleInputNotification").removeClass("alert-success");
                $("#exampleInputNotification").removeClass("alert-danger");
            }
            //Check error exist
            if ($("#exampleInputErrorTitle").css("display") === "block") {
                $("#exampleInputErrorTitle").css("display", "none");
                $("#exampleInputErrorTitle").val("");
            }
            if ($("#exampleInputErrorContent").css("display") === "block") {
                $("#exampleInputErrorContent").css("display", "none");
                $("#exampleInputErrorContent").val("");
            }
            /////////////////////////////
            $("#exampleInputNotification").css("display", "block");
            $("#exampleInputNotification").addClass("alert-success");
            $("#exampleInputNotification").val("Add chapter success");
            $("#exampleInputTitle").val("");
            $("#exampleInputContent").val("");
            ///Move to top
            $("body,html").animate({scrollTop: 0}, "slow");
        },
        error: function (response) {
            valueCss = $("#exampleInputNotification").css("display");
            if (valueCss === "block") {
                $("#exampleInputNotification").removeClass("alert-success");
                $("#exampleInputNotification").removeClass("alert-danger");
            }
            //Check error exist
            if ($("#exampleInputErrorTitle").css("display") === "block") {
                $("#exampleInputErrorTitle").css("display", "none");
                $("#exampleInputErrorTitle").val("");
            }
            if ($("#exampleInputErrorContent").css("display") === "block") {
                $("#exampleInputErrorContent").css("display", "none");
                $("#exampleInputErrorContent").val("");
            }
            ///////////////////////////////
            var errorNotification = response.responseJSON.errors;
            var fieldNameError;
            for (var i = 0; i < errorNotification.length; i++) {
                fieldNameError = errorNotification[i].fieldName;
                if (fieldNameError === "Chapter Titile") {
                    $("#exampleInputErrorTitle").css("display", "block");
                    $("#exampleInputErrorTitle").val(errorNotification[i].fieldError);
                }
                if (fieldNameError === "Chapter Content") {
                    $("#exampleInputErrorContent").css("display", "block");
                    $("#exampleInputErrorContent").val(errorNotification[i].fieldError);
                }
            }
            // console.log(response);
            // alert(JSON.stringify(response));
            $("#exampleInputNotification").css("display", "block");
            $("#exampleInputNotification").addClass("alert-danger");
            $("#exampleInputNotification").val("Add chapter fail");
            //move to top
            $("body,html").animate({scrollTop: 0}, "slow");
        }
    });
});