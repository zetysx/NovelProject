//Loading
$body = $("body");
$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
    ajaxStop: function() { $body.removeClass("loading"); }
});

$(document).ready(function () {
    var settings = {
        async: true,
        crossDomain: true,
        processData: false,
        contentType: false,
        type: 'GET',
        url: "/api/admin/genres",
        headers: {
            Accept: 'application/json'
        },
        mimeType: 'multipart/form-data'
    };
    $.ajax(settings).done(function (response) {
        var genre = JSON.parse(response.toString()).contents;
        // console.log(genre);
        // $("#exampleCheckboxGenre").append("<in")
        for (var i = 0; i < genre.length; i++) {
            $("#exampleCheckboxGenre").append("<div class='col-md-6'>" +
                "<input type=\"checkbox\" class='genreCheckbox' id=\"" + genre[i].genreName + "\" value=" + genre[i].genreId + ">" +
                "<label for=" + genre[i].genreName + ">" + genre[i].genreName + "</label>" +
                "</div>");
        }
    });
});
//Load url picture
$('#exampleInputPicture').on("change", function () {
    var $files = $(this).get(0).files;
    if ($files.length) {
        // Reject big files
        if ($files[0].size > $(this).data("max-size") * 1024) {
            console.log("Please select a smaller file");
            return false;
        }
        // Begin file upload
        console.log("Uploading file to Imgur..");
        // Replace ctrlq with your own API key
        var apiUrl = 'https://api.imgur.com/3/image';
        var apiKey = 'a0511a02359457a';
        var settings = {
            async: true,
            crossDomain: true,
            processData: false,
            contentType: false,
            type: 'POST',
            url: apiUrl,
            headers: {
                Authorization: 'Client-ID ' + apiKey,
                Accept: 'application/json'
            },
            mimeType: 'multipart/form-data'
        };
        var formData = new FormData();
        formData.append("image", $files[0]);
        settings.data = formData;
        // Response contains stringified JSON
        // Image URL available at response.data.link
        $.ajax(settings).done(function (response) {
            $("#exampleInputPicture").attr("src", JSON.parse(response.toString()).data.link);
            // console.log($("#exampleInputPicture").attr("src"));
        });
    }
});
// add novel
$("#btnAdd").click(function () {
    //Get value
    var checkedValue = [];
    $(".genreCheckbox:checked").each(function () {
        checkedValue.push($(this).val());
    });
    var novel_name = $("#exampleInputNovelName").val();
    var images = $("#exampleInputPicture").attr("src");
    var authorNovel = $("#exampleInputAuthor").val();
    var des = $("#exampleInputDescription").val();
    var year = $("#exampleInputYear").val();
    //Object
    var validate = validateField();
    if(!validate) {
        return;
    }
    var newNovel =
        {
            novelName: novel_name,
            image: images,
            author: authorNovel,
            description: des,
            genreIds: checkedValue,
            year: year
        };
    var valueCss;
    $.ajax({
        contentType: "application/json; charset=utf-8",
        type: 'POST',
        url: "/api/admin/novels",
        data: JSON.stringify(newNovel),
        success: function(response) {
            // alert(JSON.stringify(response.message));
            valueCss = $("#exampleInputNotification").css("display");
            if (valueCss === "block") {
                $("#exampleInputNotification").removeClass("alert-success");
                $("#exampleInputNotification").removeClass("alert-danger");
            }
            //Check error exist
            if ($("#exampleInputErrorNovelName").css("display") === "block") {
                $("#exampleInputErrorNovelName").css("display", "none");
                $("#exampleInputErrorNovelName").val("");
            }
            if ($("#exampleInputErrorAuthor").css("display") === "block") {
                $("#exampleInputErrorAuthor").css("display", "none");
                $("#exampleInputErrorAuthor").val("");
            }
            if ($("#exampleInputErrorGenre").css("display") === "block") {
                $("#exampleInputErrorGenre").css("display", "none");
                $("#exampleInputErrorGenre").val("");
            }
            /////////////////////////////
            $("#exampleInputNotification").css("display", "block");
            $("#exampleInputNotification").addClass("alert-success");
            $("#exampleInputNotification").val("Add novel success");
            $("#exampleInputNovelName").val("");
            $(".genreCheckbox").prop('checked', false);
            $("#exampleInputAuthor").val("");
            $("#exampleInputDescription").val("");
            $("#exampleInputYear").val("");
            $("#exampleInputPicture").val("");
            ///Move to top
            $("body,html").animate({scrollTop: 0}, "slow");
        },
        error: function(response) {
            valueCss = $("#exampleInputNotification").css("display");
            if (valueCss === "block") {
                $("#exampleInputNotification").removeClass("alert-success");
                $("#exampleInputNotification").removeClass("alert-danger");
            }
            //Check error exist
            if ($("#exampleInputErrorNovelName").css("display") === "block") {
                $("#exampleInputErrorNovelName").css("display", "none");
                $("#exampleInputErrorNovelName").val("");
            }
            if ($("#exampleInputErrorAuthor").css("display") === "block") {
                $("#exampleInputErrorAuthor").css("display", "none");
                $("#exampleInputErrorAuthor").val("");
            }
            if ($("#exampleInputErrorGenre").css("display") === "block") {
                $("#exampleInputErrorGenre").css("display", "none");
                $("#exampleInputErrorGenre").val("");
            }
            ///////////////////////////////
            var errorNotification = response.responseJSON.errors;
            var fieldNameError;
            for(var i = 0; i < errorNotification.length; i++) {
                fieldNameError = errorNotification[i].fieldName;
                if(fieldNameError === "Novel Name") {
                    $("#exampleInputErrorNovelName").css("display", "block");
                    $("#exampleInputErrorNovelName").val(errorNotification[i].fieldError);
                }
                if(fieldNameError === "Author") {
                    $("#exampleInputErrorAuthor").css("display", "block");
                    $("#exampleInputErrorAuthor").val(errorNotification[i].fieldError);
                }
                if(fieldNameError === "Novel Genres") {
                    $("#exampleInputErrorGenre").css("display", "block");
                    $("#exampleInputErrorGenre").val(errorNotification[i].fieldError);
                }
            }
            // console.log(response);
            // alert(JSON.stringify(response));
            failed();
            //move to top
            $("body,html").animate({scrollTop: 0}, "slow");
        }
    });
});

function validateField() {
    var success = true;
    var novel_name = $("#exampleInputNovelName").val().trim();
    var authorNovel = $("#exampleInputAuthor").val().trim();
    var checkedValue = [];
    $(".genreCheckbox:checked").each(function () {
        checkedValue.push($(this).val());
    });
    if(novel_name.length == 0) {
        $("#exampleInputErrorNovelName").css("display", "block");
        $("#exampleInputErrorNovelName").val("Field cannot be blank or empty");
        failed();
        success = false;
    } else {
        $("#exampleInputErrorNovelName").css("display", "none");
    }
    if(authorNovel.length == 0) {
        $("#exampleInputErrorAuthor").css("display", "block");
        $("#exampleInputErrorAuthor").val("Field cannot be blank or empty");
        failed();
        success = false;
    } else {
        $("#exampleInputErrorAuthor").css("display", "none");
    }
    if(checkedValue.length == 0) {
        $("#exampleInputErrorGenre").css("display", "block");
        $("#exampleInputErrorGenre").val("Novel must have at least 1 genre");
        failed();
        success = false;
    } else {
        $("#exampleInputErrorGenre").css("display", "none");
    }
    return success;
}

function failed() {
    $("#exampleInputNotification").css("display", "block");
    $("#exampleInputNotification").addClass("alert-danger");
    $("#exampleInputNotification").val("Add novel fail");
}
