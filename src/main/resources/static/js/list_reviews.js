var table;
var index = 1;
var reviewId;
$body = $("body");
$(document).on({
    ajaxStart: function () {
        $body.addClass("loading");
    },
    ajaxStop: function () {
        $body.removeClass("loading");
    }
});
$(document).ready(function () {
    table = $("#listReviews").DataTable({
        columnDefs: [{
            searchable: false,
            orderable: true,
            targets: 0
        }],
        order: [[0, 'asc']],
        processing: true,
        ajax: {
            url: "/api/admin/reviews",
            dataSrc: "contents"
        },
        columns: [
            {data: processIndex, searchable: false},
            {data: "novelName", searchable: true},
            {data: "username", searchable: true},
            {data: "rating", searchable: false},
            {data: "review", searchable: true},
            {data: processDataButton, searchable: false}
        ]

    });
    function processIndex(data, type, full, meta) {
        return '<span>'+ index++ + '</span>';
    }
    function processDataButton(data, type, full, meta) {
        return '<button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" id="changeStatus" value=' + data.reviewId + '>Delete</button>';
    }
    $('#listReviews tbody').on('click', 'tr', function () {
        reviewId = table.row(this).data().reviewId;
    });
    $("#deleteReview").click(function () {
        $.ajax({
            type: 'DELETE',
            url: "/api/admin/reviews/" + reviewId,
            success: function (response) {
                table.ajax.reload();
                location.reload();
            }
        })
    });
});
