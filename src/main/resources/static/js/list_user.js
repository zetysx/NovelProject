//Loading
$body = $("body");
$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
    ajaxStop: function() { $body.removeClass("loading"); }
});
var table;
var user;
$(document).ready(function () {
    table = $('#listUserTable').DataTable({
        processing: true,
        ajax: {
            url: "/api/admin/listUsers",
            dataSrc: "contents"
        },
        columns: [
            {data: processDataAvartar, searchable: false},
            {data: "username", searchable: true},
            {data:"fullname", searchable: true},
            {data: "sex", searchable: true},
            {data: "birthday", searchable: true},
            {data: "enable", searchable: true},
            {data: processDataButton, searchable: true}
        ],
        rowCallback:  function( row, data ) {
            if ( data.enable == true ) {
                $(row).addClass("table-light");
            } else {
                $(row).addClass("table-danger");
            }
        }
    });
    function processDataAvartar(data, type, full, meta) {
        if(data.avatar != null){
            return '<img src="' + data.avatar +'"alt="Avatar" width="150" height="130">';
        }
        return '';
    }
    function processDataButton(data, type, full, meta) {
        if (data.enable === true) {
            return '<button class="btn btn-danger" data-toggle="modal" data-target="#banModal" id="changeStatus" value=' + data.username + '>Ban</button>';
        } else {
            return '<button class="btn btn-success" data-toggle="modal" data-target="#unbanModal" id="changeStatus" value=' + data.username + '>Unban</button>';
        }
    }
    $('#listUserTable tbody').on('click', 'tr', function () {
        user = table.row(this).data().username;
    });
    $("#unbanUser").click(function () {
        $.ajax({
            type: 'POST',
            url: "/api/admin/users/" + user,
            headers: {
                Accept: 'application/json'
            },
            success: function (response) {
                table.ajax.reload();
            }
        });
    });
    $("#banUser").click(function () {
        var banDto = {
            banTime: $("#banTime").val()
        };
        $.ajax({
            contentType: "application/json; charset=utf-8",
            type: 'POST',
            url: "/api/admin/users/" + user,
            data: JSON.stringify(banDto),
            headers: {
                Accept: 'application/json'
            },
            success: function (response) {
                table.ajax.reload();
            },
            error: function (res) {
                console.log(res);
            }
        });
    });
});
