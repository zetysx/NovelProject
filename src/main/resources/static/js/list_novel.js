//Loading
$body = $("body");
$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
    ajaxStop: function() { $body.removeClass("loading"); }
});
var table;
var novelId;
$(document).ready(function () {
    table = $('#listNovelTable').DataTable({
        processing: true,
        ajax: {
            url: "/api/admin/novels/",
            dataSrc: "contents"
        },
        columns: [
            {data: processDataNovelName, searchable: true},
            {data: "author", searchable: true},
            {data: "rating", searchable: true},
            {data: processDataChapter, searchable: true},
            {data: "year", searchable: true},
            {data: "active", searchable: true},
            {data: processDataButton, searchable: false}
        ],
        rowCallback:  function( row, data ) {
            if ( data.active == true ) {
                $(row).addClass("table-light");
            } else {
                $(row).addClass("table-danger");
            }
        }
    });
    $('#listNovelTable tbody').on('click', 'tr', function () {
        novelId = table.row(this).data().novelId;
    });
    $("#deleteNovel").click(function () {
        $.ajax({
            type: 'POST',
            url: "/api/admin/delete/" + novelId,
            headers: {
                Accept: 'application/json'
            },
            success: function (response) {
                table.ajax.reload();
            }
        });
    });

    // function processDataView(data, type, full, meta) {
    //
    //     return '<a href="admin/reviews/">' + data.rating + '</a>';
    // }

    function processDataNovelName(data, type, full, meta) {
        return '<a href="admin/novels/details/' + data.novelId + '">' + data.novelName + '</a>';
    }

    function processDataChapter(data, type, full, meta) {
        return '<a href="admin/novels/chapter/' + data.novelId + '">' + data.chapterCount + '</a>';
    }

    function processDataButton(data, type, full, meta) {
        if (data.active === true) {
            return '<button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" id="changeStatus" value=' + data.novelId + '>Dective</button>';
        } else {
            return '<button class="btn btn-success" data-toggle="modal" data-target="#deleteModal" id="changeStatus" value=' + data.novelId + '>Active</button>';
        }
    }
});
