$body = $("body");
$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
    ajaxStop: function() { $body.removeClass("loading"); }
});

$(document).ready(function () {
    var username = $("#username").html();
    $("#btnChange").click(function () {
        var validate = true;
        if ($("#newPass").val().trim().length < 6) {
            $("#errorNewPass").css("display", "block");
            validate = false;
        } else {
            $("#errorNewPass").css("display", "none");
        }
        if ($("#retypePass").val().trim() != $("#newPass").val().trim()) {
            $("#errorRetypePass").css("display", "block");
            validate = false;
        } else {
            $("#errorRetypePass").css("display", "none");
        }
        if (!validate) {
            return;
        }
        var changePassDTO = {
            oldPass: $("#oldPass").val(),
            newPass: $("#newPass").val()
        }
        $.ajax({
            contentType: "application/json; charset=utf-8",
            type: 'POST',
            url: '/api/admin/details/' + username,
            data: JSON.stringify(changePassDTO),
            success: function (response) {
                $(".error").css("display", "none");
                $("#oldPass").val("");
                $("#newPass").val("");
                $("#retypePass").val("");
                $("#success").css("display","block");
            },
            error: function (response) {
                var errorNotification = response.responseJSON.errors;
                var fieldNameError;
                for(var i = 0; i < errorNotification.length; i++) {
                    fieldNameError = errorNotification[i].fieldName;
                    if(fieldNameError === "Old Password") {
                        $("#errorOldPass").css("display", "block");
                        $("#errorOldPass").val(errorNotification[i].fieldError);
                    }
                    if(fieldNameError === "New Password") {
                        $("#errorNewPass").css("display", "block");
                        $("#errorNewPass").val(errorNotification[i].fieldError);
                    }
                }
            }
        })
    });
});