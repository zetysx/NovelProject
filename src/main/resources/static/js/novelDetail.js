//Loading
$body = $("body");
$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
    ajaxStop: function() { $body.removeClass("loading"); }
});

var novelId;
var detailNovel;
$(document).ready(function () {
    $(".edit").css("display", "none");
    novelId = $("#novel_id").val();
    var settings = {
        async: true,
        crossDomain: true,
        processData: false,
        contentType: false,
        type: 'GET',
        url: "/api/admin/novels/" + novelId,
        headers: {
            Accept: 'application/json'
        }
    };

    $.ajax(settings).done(function (response) {
        detailNovel = response.contents;
        console.log(response.contents);
        var coverNovel = detailNovel.cover;
        var nameNovel =  detailNovel.novelName;
        var authorNovel = detailNovel.author;
        var ratingNovel = detailNovel.ratings;
        var statusTranslateNovel = detailNovel.statusTranslate;
        var viewNovel = detailNovel.views;
        var yearNovel = detailNovel.year;
        var descriptionNovel = detailNovel.description;
        var listGenreNovel = detailNovel.genres;
        var activeNovel = detailNovel.active;
        //Cover
        $("#detailCover").attr("src", coverNovel);
        //Novel name
        $("#detailNovelName").text(nameNovel);
        //Author
        $("#detailAuthor").text(authorNovel);
        //Year
        $("#detailYear").text(yearNovel);
        //status translate
        $("#detailStatusTranslate").text(statusTranslateNovel);
        $("#editTranslateSelect").val(statusTranslateNovel);
        //Rating
        $("#detailRatings").text(ratingNovel);
        //View
        $("#detailView").text(viewNovel);
        //Description
        $("#detailDescription").text(descriptionNovel);
        //Genres
        var genreNovel = '';
        for(var i = 0; i < listGenreNovel.length; i++) {
            genreNovel = genreNovel + listGenreNovel[i].genreName;
            if(i != listGenreNovel.length - 1) {
                genreNovel = genreNovel + ", ";
            }
        }
        $("#detailGenres").text(genreNovel);
        //Active
        $("#detailActive").text(activeNovel);

        $("#editName").val(nameNovel);
        $("#editAuthor").val(authorNovel);
        $("#editYear").val(yearNovel);
        $("#editTranslate").val(statusTranslateNovel);
        $("#editDes").val(descriptionNovel);
        var genre = {
            async: true,
            crossDomain: true,
            processData: false,
            contentType: false,
            type: 'GET',
            url: "/api/admin/genres",
            headers: {
                Accept: 'application/json'
            },
            mimeType: 'multipart/form-data'
        };
        $.ajax(genre).done(function (response) {
            var genre = JSON.parse(response.toString()).contents;
            // console.log(genre);
            // $("#exampleCheckboxGenre").append("<in")
            for (var i = 0; i < genre.length; i++) {
                $("#checkboxGenre").append("<div class='col-md-6 checkbox-group required'>" +
                    "<input type=\"checkbox\" class='genreCheckbox' id=\"" + genre[i].genreName + "\" value=" + genre[i].genreId + ">" +
                    "<label for=" + genre[i].genreName + ">" + genre[i].genreName + "</label>" +
                    "</div>");
            }
            for (var i = 0; i < listGenreNovel.length; i++) {
                var genreId = listGenreNovel[i].genreId;
                $(".genreCheckbox").each(function() {
                    if($(this).val() == genreId) {
                        $(this).attr("checked", "checked");
                    }
                });
            }
        });

    });
    $("#btnEdit").click(function() {
        if ($(".edit").css("display")=="none"){
            $("#btnEdit").html("Cancel");
            $("#btnEdit").attr("class", "btn btn-danger btn-block");
            $(".edit").css("display", "block");
            $(".detail").css("display", "none");
        } else {
            $("#btnEdit").html("Edit");
            $("#btnEdit").attr("class", "btn btn-primary btn-block");
            $(".edit").css("display", "none");
            $(".detail").css("display", "block");
        }
    });
    $("#btnUpdate").click(function() {
        $("#editName").val($("#editName").val().trim());
        //$("#editName")[0].setCustomValidity('Novel name must be between 3 and 50 characters');
        $("#editAuthor").val($("#editAuthor").val().trim());
        if($('div.checkbox-group.required :checkbox:checked').length <= 0) {
            $("#errorGenre").css("display", "block");
            return;
        } else {
            $("#errorGenre").css("display", "none");
        }
        //$("#editAuthor")[0].setCustomValidity('Author is required');
        $("#form")[0].checkValidity();
        $("#form").find(':submit').click();
        //Call function edit
        editNovel(novelId);
    })
});
$('#editPicture').on("change", function () {
    var $files = $(this).get(0).files;
    if ($files.length) {
        // Reject big files
        if ($files[0].size > $(this).data("max-size") * 1024) {
            console.log("Please select a smaller file");
            return false;
        }
        // Begin file upload
        console.log("Uploading file to Imgur..");
        // Replace ctrlq with your own API key
        var apiUrl = 'https://api.imgur.com/3/image';
        var apiKey = 'a0511a02359457a';
        var upload = {
            async: true,
            crossDomain: true,
            processData: false,
            contentType: false,
            type: 'POST',
            url: apiUrl,
            headers: {
                Authorization: 'Client-ID ' + apiKey,
                Accept: 'application/json'
            },
            mimeType: 'multipart/form-data'
        };
        var formData = new FormData();
        formData.append("image", $files[0]);
        upload.data = formData;
        // Response contains stringified JSON
        // Image URL available at response.data.link
        $.ajax(upload).done(function (response) {
            $("#detailCover").attr("src", JSON.parse(response.toString()).data.link);
            // console.log($("#exampleInputPicture").attr("src"));
        });
    }
});
function editNovel(novelId) {
    //Get value
    var checkedValue = [];
    $(".genreCheckbox:checked").each(function () {
        checkedValue.push($(this).val());
    });
    var novel_name = $("#editName").val();
    var images = $("#detailCover").attr("src");
    var authorNovel = $("#editAuthor").val();
    var des = $("#editDes").val();
    var year = $("#editYear").val();
    var translateStatus = $("#editTranslateSelect").val();
    //Object
    var validate = validateField();
    if(!validate) {
        return;
    }
    var editNovel =
        {
            novelName: novel_name,
            image: images,
            author: authorNovel,
            description: des,
            genreIds: checkedValue,
            year: year,
            statusTranslate: translateStatus
        };
    $.ajax({
        contentType: "application/json; charset=utf-8",
        type: 'POST',
        url: "/api/admin/novels/" + novelId,
        data: JSON.stringify(editNovel),
        success: function(response) {
            console.log("edit" + response);
            //Check error exist
            if ($("#exampleInputErrorNovelName").css("display") === "block") {
                $("#exampleInputErrorNovelName").css("display", "none");
                $("#exampleInputErrorNovelName").val("");
            }
            if ($("#exampleInputErrorAuthor").css("display") === "block") {
                $("#exampleInputErrorAuthor").css("display", "none");
                $("#exampleInputErrorAuthor").val("");
            }
            if ($("#errorGenre").css("display") === "block") {
                $("#errorGenre").css("display", "none");
                $("#errorGenre").val("");
            }
            if ($("#exampleInputErrorStatusTransalte").css("display") === "block") {
                $("#exampleInputErrorStatusTransalte").css("display", "none");
                $("#exampleInputErrorStatusTransalte").val("");
            }
            /////////////////////////////
            ///Move to top
            $("body,html").animate({scrollTop: 0}, "slow");
        },
        error: function(response) {
            //Check error exist
            if ($("#exampleInputErrorNovelName").css("display") === "block") {
                $("#exampleInputErrorNovelName").css("display", "none");
                $("#exampleInputErrorNovelName").val("");
            }
            if ($("#exampleInputErrorAuthor").css("display") === "block") {
                $("#exampleInputErrorAuthor").css("display", "none");
                $("#exampleInputErrorAuthor").val("");
            }
            if ($("#errorGenre").css("display") === "block") {
                $("#errorGenre").css("display", "none");
                $("#errorGenre").val("");
            }
            if ($("#exampleInputErrorStatusTransalte").css("display") === "block") {
                $("#exampleInputErrorStatusTransalte").css("display", "none");
                $("#exampleInputErrorStatusTransalte").val("");
            }
            ///////////////////////////////
            var errorNotification = response.responseJSON.errors;
            var fieldNameError;
            for(var i = 0; i < errorNotification.length; i++) {
                fieldNameError = errorNotification[i].fieldName;
                if(fieldNameError === "Novel Name") {
                    $("#exampleInputErrorNovelName").css("display", "block");
                    $("#exampleInputErrorNovelName").val(errorNotification[i].fieldError);
                }
                if(fieldNameError === "Author") {
                    $("#exampleInputErrorAuthor").css("display", "block");
                    $("#exampleInputErrorAuthor").val(errorNotification[i].fieldError);
                }
                if(fieldNameError === "Novel Genres") {
                    $("#errorGenre").css("display", "block");
                    $("#errorGenre").val(errorNotification[i].fieldError);
                }
                if(fieldNameError === "Status Translates") {
                    $("#exampleInputErrorStatusTransalte").css("display", "block");
                    $("#exampleInputErrorStatusTransalte").val(errorNotification[i].fieldError);
                }
            }
            // console.log(response);
            // alert(JSON.stringify(response));

            //move to top
            $("body,html").animate({scrollTop: 0}, "slow");
        }
    });
}
function validateField() {
    var success = true;
    var novel_name = $("#editName").val().trim();
    var authorNovel = $("#editAuthor").val().trim();
    var translateStatus = $("#editTranslateSelect").val().trim();
    var checkedValue = [];
    $(".genreCheckbox:checked").each(function () {
        checkedValue.push($(this).val());
    });

    if(novel_name.length == 0) {
        $("#exampleInputErrorNovelName").css("display", "block");
        $("#exampleInputErrorNovelName").val("Field cannot be blank or empty");
        success = false;
    } else {
        $("#exampleInputErrorNovelName").css("display", "none");
    }
    if(authorNovel.length == 0) {
        $("#exampleInputErrorAuthor").css("display", "block");
        $("#exampleInputErrorAuthor").val("Field cannot be blank or empty");
        success = false;
    } else {
        $("#exampleInputErrorAuthor").css("display", "none");
    }
    if(checkedValue.length == 0) {
        $("#errorGenre").css("display", "block");
        $("#errorGenre").val("Novel must have at least 1 genre");
        success = false;
    } else {
        $("#exampleInputErrorGenre").css("display", "none");
    }
    if(translateStatus.length == 0) {
        $("#exampleInputErrorStatusTransalte").css("display", "block");
        $("#exampleInputErrorStatusTransalte").val("Status Translated can not be empty!");
        success = false;
    } else {
        $("#exampleInputErrorStatusTransalte").css("display", "none");
    }
    return success;
}