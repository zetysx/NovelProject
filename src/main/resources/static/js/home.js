$(document).ready(function() {
    var table = $('#novelsTable').DataTable( {
        "processing" : true,
        "ajax": {
            "url": "/api/admin/novels/",
            "dataSrc": "contents"
        },
        "columns": [
            { "data": "cover","searchable": false,
                render : function( data, type, full, meta ) {
                    return type == 'display' ? '<img width="90px" height="90px" src="'+ data + '"/>' : data} },
            { "data": "novelName", "searchable": true },
            { "data": "author" },
            { "data": "novelId",  "searchable": false,
                render : function( data, type, full, meta ) {
                    return type == 'display' ? '<a class="btn btn-info btn-sm" href="admin/novels/chapter/'+ data + '">Chapters page</a>' : data} },
            { "data": "novelId", "searchable": false,
                render : function( data, type, full, meta ) {
                    return type == 'display' ? '<a class="btn btn-info btn-sm" href="admin/novels/reviews/'+ data + '">Reviews page</a>' : data} },
        ]
    } );
    console.log("SUCCESS : ");
});
