package com.mobile.project.Constant;

public enum StatusCode {

    CODE200(200, "OK"),
    CODE400(400, "BAD REQUEST"),
    CODE401(401, "UNAUTHORIZED"),
    CODE404(404, "NOT FOUND"),
    CODE403(403, "FORBIDDEN"),
    CODE500(500, "INTERNAL SERVER ERROR");

    private int code;
    private String massage;

    StatusCode(int code, String massage) {
        this.code = code;
        this.massage = massage;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    @Override
    public String toString() {
        return code + ", " + massage + '\'';
    }
}
