package com.mobile.project.Constant;

public class Const {
    //API
// ==========================================================================================================
    public static final String ROOT_AUTHORIZATION = "/api/authorization";
    public static final String API_LOGIN = "/login";

    //admin
    public static final String ROOT_API_FOR_ADMIN = "/api/admin";
    public static final String API_DEACTIVATE_NOVEL = "/delete/{novel_id}";
    public static final String API_ADD_NOVEL = "/novels";
    public static final String API_ADD_CHAPTER = "/chapters";
    public static final String API_LIST_USER = "/listUsers";
    public static final String API_LIST_REVIEWS_ADMIN = "/reviews";
    public static final String API_DELETE_REVIEW_ADMIN = "/reviews/{review_id}";
    public static final String API_DEACTIVE_USER = "/users/{username}";

    public static final String API_GENRES = "/genres";
    public static final String API_USER_DETAIL = "/details/{username}";
    public static final String API_LIST_NOVELS = "/novels";
    public static final String API_LIST_CHAPTERS = "/novels/{novel_id}/chapters";
    public static final String API_LIST_REVIEWS = "/reviews/{novel_id}";
    //user
    public static final String ROOT_API_FOR_USER = "/api/user";
    public static final String API_ADD_REVIEW = "/review";
    public static final String API_DELETE_REVIEW = "/review/delete/{novel_id}";
    public static final String API_NOVEL_DETAIL = "/novels/{novel_id}";
    public static final String API_CHAPTER_DETAIL = "/chapters/{chapter_id}";
    public static final String API_ADD_FAVORITE = "/favorite/{novel_id}";
    public static final String API_LIST_FAVORITE = "/favorites";
    public static final String API_GENRE_NOVEL = "/genres/{genre_id}";
    public static final String API_REGISTER = "/register";
// ==========================================================================================================
    public static final String DEFAULT_URL_FOR_COVER="https://i.imgur.com/7lnivdz.jpg";
    public static final String DEFAULT_URL_FOR_USER="https://i.imgur.com/vWqbS9Q.jpg";
    public static final String DEFAULT_DESCRIPTION="updating";

    public static final String SQL_QUERY_USERNAME_PASSWORD="select username,password,enabled from Account where username=?";
    public static final String SQL_QUERY_USERNAME_ROLE="select username, role from Account where username=?";


    public static final String ROLE_USER="ROLE_USER";
    public static final String ROLE_ADMIN="ROLE_ADMIN";
    public static final String DATE_TIME_PATTERN = "dd-MM-yyyy HH:mm:ss";
    public static final String DATE_TIME_PATTERN_BAN = "dd-MM-yyyy HH:mm";
    public static final String DATE_JSON_PATTERN = "yyyy-MM-dd";
    public static final String DATE_PATTERN = "dd-MM-yyyy";
    public static final String DEFAULT_SORT_NOVELS =  "novelName";
    public static final String DEFAULT_LAST_UPDATED =  "01-01-2018 15:30:45";
    public static final String ONGOING =  "Ongoing";
    public static final String COMPLETE =  "Complete";
    public static final String SUFFIX_TINY = "s";
    public static final String SUFFIX_SMALL = "b";
    public static final String SUFFIX_MEDIUM = "m";

    public static final double DEFAULT_RATING = 0.0;
    public static final double MIN_RATING = 0.0;
    public static final double MAX_RATING = 5.0;
    public static final double INT_TO_DOUBLE = 1.0;
    public static final int DEFAULT_VIEW = 0;
    public static final int DEFAULT_YEAR = 2010;
    public static final int MIN_LENGTH_USERNAME = 6;
    public static final int MAX_LENGTH_USERNAME = 30;
    public static final int MIN_LENGTH_PASSWORD = 6;
    public static final int MAX_LENGTH_PASSWORD = 30;
    public static final int MIN_BIRTHDAY_YEAR = 2013;

    public static final String TIME_ZONE_ID = "Asia/Ho_Chi_Minh";
    public static final String BANNED_MESSAGE = "You have been banned! \n Your ban will be lifted at ";
    public static final String BAN_TIME = "Can not unban this user";
    public static final String REVIEW_ADDEDD_SUCESSFULLY = "Review added successfully!";
//    ========================================================================================================================================
    //Error
    public static final String ERROR_MESSAGE = "Field can not be blank or empty";
    public static final String ERROR_USERNAME_PASSWORD = "Wrong password";
    public static final String ERROR_OLD_PASSWORD = "Old Password";
    public static final String ERROR_NEW_PASSWORD = "New Password";
    public static final String ERROR_REQUEST_BODY = "Request Body";
    public static final String ERROR_URL_WRONG_TYPE = "Type mismatch";
    public static final String ERROR_REQUEST_NOT_FOUND = "No such Request";
    //review
    public static final String ERROR_REVIEW_USERNAME = "Username";
    public static final String ERROR_REVIEW_NOVELID = "NovelID";
    public static final String ERROR_REVIEW_CONTENT = "Review";
    public static final String ERROR_REVIEW_RATING = "Rating";
    public static final String ERROR_REVIEW = "User have not review this novel yet!";
    public static final String ERROR_REVIEW_ADMIN = "There is no review with this ID!";
    //novel
    public static final String ERROR_NOVEL_NAME = "Novel Name";
    public static final String ERROR_NOVEL_AUTHOR = "Author";
    public static final String ERROR_NOVEL_GENRE = "Novel Genres";
    public static final String ERROR_NOVEL_STATUS_TRANSLATED = "Status Translated";
    public static final String ERROR_MESSAGE_STATUS_TRANSLATED = "Status Translated can not be empty!";
    public static final String ERROR_MESSAGE_STATUS_TRANSLATED_FORMAT = "Status Translated is in wrong format!";
    public static final String ERROR_MESSAGE_NOVEL_ID = "There is no novel with this ID";
    public static final String ERROR_MESSAGE_RATING = "Rating must be between 0 and 5";
    public static final String ERROR_MESSAGE_GENRE = "Novel must have at least 1 genre";
    //chapter
    public static final String ERROR_NOVEL_ID = "Novel ID";
    public static final String ERROR_CHAPTER_ID = "Chapter ID";
    public static final String ERROR_CHAPTER_TITLE = "Chapter Titile";
    public static final String ERROR_CHAPTER_CONTENT = "Chapter Content";
    public static final String ERROR_MESSAGE_CHAPTER_ID = "There is no such chapter";
    //favorite
    public static final String ERROR_USERNAME = "username";
    public static final String ERROR_USERNAME_MESSAGE = "There is no user with this username";
    public static final String ERROR_USERNAME_DUPLICATE = "Username already exists!";
    public static final String ERROR_BIRTHDATE = "birthday";
    public static final String ERROR_BIRTHDATE_MESSAGE = "Birthday is in wrong format";
    public static final String ERROR_GENDER = "gender";
    public static final String ERROR_PASSWORD = "password";
    public static final String ERROR_PASSWORD_BLANK = "Password can not be blank!";
    public static final String ERROR_PASSWORD_LENGTH = "Password must be between 6 and 30 characters!";
    public static final String ERROR_USERNAME_BLANK = "Username can not be blank!";
    public static final String ERROR_USERNAME_LENGTH = "Username must be between 6 and 30 characters!";
    public static final String ERROR_GENDER_BLANK = "Gender can not be blank!";
    public static final String ERROR_BIRTHDAY_YEAR = "Birthday must be under 2013!";
//    ========================================================================================================
//   Web admin
    public static final String ADMIN="ADMIN";
    public static final String LOGIN_FAILURE_URL="/login?error=true";
    public static final String ACCESS_DENIED = "/403";
    public static final String MATCHER="/**";
    public static final String ADMIN_LIST_NOVEL = "/admin";
    public static final String LOGOUT = "/logout";
    public static final String ADMIN_LOGIN = "/login";
    public static final String ADMIN_ADD_NOVEL = "/addNovel";
    public static final String ADMIN_LIST_CHAPTER = "admin/novels/chapter/{novel_id}";
    public static final String ADMIN_ADD_CHAPTER = "admin/novels/chapter/addChapter/{novel_id}";
    public static final String ADMIN_DETAIL_NOVEL = "admin/novels/details/{novel_id}";
    public static final String ADMIN_LIST_USER = "/admin/users";
    public static final String ADMIN_LIST_REVIEWS = "/admin/reviews";
    public static final String ADMIN_DETAIL = "/admin/{username}";
}
