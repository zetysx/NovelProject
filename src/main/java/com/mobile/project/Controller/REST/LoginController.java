package com.mobile.project.Controller.REST;

import com.mobile.project.Constant.Const;
import com.mobile.project.Constant.StatusCode;
import com.mobile.project.DTO.Response.StatusCodeDTO;
import com.mobile.project.Service.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Const.ROOT_AUTHORIZATION)
public class LoginController {

    @Autowired
    AuthorizationService authorizationService;

    /**
     * authenticate and authorize user
     * @param username
     * @param password
     * @return
     */
    @RequestMapping(value = Const.API_LOGIN, method = RequestMethod.POST)
    public ResponseEntity<Object> checkLogin(@RequestHeader("username") String username, @RequestHeader("password") String password) {
        boolean valid = authorizationService.checkValid(username, password);
        if(valid) {
            String ban = authorizationService.checkBanned(username);
            if(ban!=null) {
                StatusCodeDTO dto = new StatusCodeDTO();
                dto.setCode(StatusCode.CODE403.getCode());
                dto.setMessage(ban);
                return new ResponseEntity<>(dto, HttpStatus.FORBIDDEN);
            }
        } else {
            return new ResponseEntity<>(new StatusCodeDTO(StatusCode.CODE401), HttpStatus.UNAUTHORIZED);
        }
        boolean authorized = authorizationService.checkLogin(username, password, Const.ROLE_USER);
        return authorized ? new ResponseEntity<>(new StatusCodeDTO(StatusCode.CODE200), HttpStatus.OK) : new ResponseEntity<>(new StatusCodeDTO(StatusCode.CODE401), HttpStatus.UNAUTHORIZED);
    }
}
