package com.mobile.project.Controller.REST;

import com.mobile.project.Constant.Const;
import com.mobile.project.Constant.StatusCode;
import com.mobile.project.DTO.*;
import com.mobile.project.DTO.Error.ErrorResponseDTO;
import com.mobile.project.DTO.Error.FieldErrorDTO;
import com.mobile.project.DTO.Response.ResponseWithStatusCodeDTO;
import com.mobile.project.DTO.Response.StatusCodeDTO;
import com.mobile.project.Service.AccountService;
import com.mobile.project.Service.ChapterService;
import com.mobile.project.Service.NovelService;
import com.mobile.project.Service.ReviewService;
import com.mobile.project.Utils.Utils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mobile.project.Constant.StatusCode.CODE200;

@RestController
@RequestMapping(Const.ROOT_API_FOR_USER)
public class UserNovelController {

    private static final Logger logger = LoggerFactory.getLogger(UserNovelController.class);

    @Autowired
    NovelService novelService;

    @Autowired
    ReviewService reviewService;

    @Autowired
    ChapterService chapterService;

    @Autowired
    AccountService accountService;

    /**
     * get list novels
     * @param sortBy
     * @param search
     * @return
     */
    @RequestMapping(value = Const.API_LIST_NOVELS, method = RequestMethod.GET)
    public ResponseEntity<Object> getAllNovels(@RequestParam(value = "sortBy", required = false) String sortBy, @RequestParam(value = "search", required = false) String search) {
        List<MiniNovelDTO> dtos = null;
        if (sortBy == null) {
            sortBy = Const.DEFAULT_SORT_NOVELS;
        }
        if (search != null) {
            dtos = novelService.searchNovel(search, sortBy);
        } else {
            dtos = novelService.findNovelMobile(sortBy);
        }
        ResponseWithStatusCodeDTO response = new ResponseWithStatusCodeDTO();
        response.setStatus(CODE200.getCode());
        response.setContents(dtos);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    /**
     * add review made by user
     * @param userReviewDTO
     * @return
     */
    @RequestMapping(value = Const.API_ADD_REVIEW, method = RequestMethod.POST)
    public ResponseEntity<Object> addReview(@RequestBody UserReviewDTO userReviewDTO) {
        List<FieldErrorDTO> errors = checkReviewErrors(userReviewDTO);
        if (!errors.isEmpty()) {
            return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE400, errors), HttpStatus.BAD_REQUEST);
        }
        FieldErrorDTO responseStr = reviewService.addNewReview(userReviewDTO);
        if (responseStr.getFieldError().equals(Const.REVIEW_ADDEDD_SUCESSFULLY))
            return new ResponseEntity<Object>(new StatusCodeDTO(CODE200), HttpStatus.OK);
        List<FieldErrorDTO> error = new ArrayList<>();
        error.add(responseStr);
        return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE500, error), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * get novel detail along with user-related info with the novel( have user added this novel to favorite, have user reviewed this novel)
     * @param novelId
     * @param username
     * @return
     */
    @RequestMapping(value = Const.API_NOVEL_DETAIL, method = RequestMethod.GET)
    public ResponseEntity<Object> viewDetail(@PathVariable("novel_id") int novelId, @RequestHeader(value = "username", required = false) String username) {
        NovelDTO novelDto = novelService.viewNovelDetail(novelId);
        NovelWIthFavoriteDTO dto = new NovelWIthFavoriteDTO(novelDto);
        if (username != null) {
            boolean isFavorite = novelService.isFavorite(novelId, username);
            dto.setFavorite(isFavorite);
            RatingForNovelDetailDTO ratingDTO = reviewService.isReviewed(novelId, username);
            dto.setReviewed(ratingDTO.isReview());
            dto.setRatingForUser(ratingDTO.getRating());
            dto.setReviewUser(ratingDTO.getReview());
        }
        ResponseWithStatusCodeDTO response = new ResponseWithStatusCodeDTO();
        if (novelDto != null) {
            response.setStatus(CODE200.getCode());
            response.setContents(dto);
            return new ResponseEntity<Object>(response, HttpStatus.OK);
        }
        return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE500), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * gegt list chapter of a novel
     * @param novelId
     * @return
     */
    @RequestMapping(value = Const.API_LIST_CHAPTERS, method = RequestMethod.GET)
    public ResponseEntity<Object> getListChapters(@PathVariable("novel_id") int novelId) {
        List<MiniChapterDTO> dtos = chapterService.getListChapters(novelId);
        ResponseWithStatusCodeDTO response = new ResponseWithStatusCodeDTO();
        response.setStatus(CODE200.getCode());
        response.setContents(dtos);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    /**
     * get a detailed chapter
     * @param chapterId
     * @return
     */
    @RequestMapping(value = Const.API_CHAPTER_DETAIL, method = RequestMethod.GET)
    public ResponseEntity<Object> getChapterDetail(@PathVariable("chapter_id") int chapterId) {
        ChapterDTO dto = chapterService.getOneChapter(chapterId);
        if (dto != null) {
            ResponseWithStatusCodeDTO response = new ResponseWithStatusCodeDTO();
            response.setStatus(CODE200.getCode());
            response.setContents(dto);
            return new ResponseEntity<Object>(response, HttpStatus.OK);
        }
        return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE500), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * add a novel to user's favorite list
     * @param novelId
     * @param username
     * @return
     */
    @RequestMapping(value = Const.API_ADD_FAVORITE, method = RequestMethod.POST)
    public ResponseEntity<Object> addFavorite(@PathVariable("novel_id") int novelId, @RequestHeader("username") String username) {
        FieldErrorDTO addFavorite = novelService.addFavorite(username, novelId);
        if (addFavorite == null) {
            return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE200), HttpStatus.OK);
        }
        List<FieldErrorDTO> errors = new ArrayList<>();
        errors.add(addFavorite);
        ErrorResponseDTO dto = new ErrorResponseDTO(StatusCode.CODE500, errors);
        return new ResponseEntity<Object>(dto, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * get list of a review of of a novel
     * @param novelId
     * @return
     */
    @RequestMapping(value = Const.API_LIST_REVIEWS, method = RequestMethod.GET)
    public ResponseEntity<Object> getReviewNovel(@PathVariable("novel_id") int novelId) {
        List<ReviewNovelDTO> reviews = reviewService.getReviewsNovel(novelId);
        ResponseWithStatusCodeDTO response = new ResponseWithStatusCodeDTO();
        response.setStatus(CODE200.getCode());
        response.setContents(reviews);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    /**
     * get user's favorite novel list
     * @param username
     * @return
     */
    @RequestMapping(value = Const.API_LIST_FAVORITE, method = RequestMethod.GET)
    public ResponseEntity<Object> getFavorites(@RequestHeader("username") String username) {
        List<MiniNovelDTO> novels = novelService.getListFavorites(username);
        ResponseWithStatusCodeDTO response = new ResponseWithStatusCodeDTO();
        response.setStatus(CODE200.getCode());
        response.setContents(novels);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    /**
     * get list of available genres
     * @return
     */
    @RequestMapping(value = Const.API_GENRES, method = RequestMethod.GET)
    public ResponseEntity<Object> getGenres() {
        ResponseWithStatusCodeDTO dto = new ResponseWithStatusCodeDTO();
        List<GenreUserDTO> genres = novelService.getListGenreUser();
        if (!Utils.isNullOrEmpty(genres)) {
            dto.setStatus(StatusCode.CODE200.getCode());
            dto.setContents(genres);
            return new ResponseEntity<Object>(dto, HttpStatus.OK);
        }
        return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE500), HttpStatus.OK);
    }

    /**
     * get list of novels belong to 1 genre
     * @param genreId
     * @return
     */
    @RequestMapping(value = Const.API_GENRE_NOVEL, method = RequestMethod.GET)
    public ResponseEntity<Object> getListNovelByGenre(@PathVariable("genre_id") Integer genreId) {
        List<MiniNovelDTO> dtos = null;
        dtos = novelService.getListNovelByGenre(genreId);
        ResponseWithStatusCodeDTO response = new ResponseWithStatusCodeDTO();
        response.setStatus(CODE200.getCode());
        response.setContents(dtos);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    /**
     * delete a review made by user in the past
     * @param novelID
     * @param username
     * @return
     */
    @RequestMapping(value = Const.API_DELETE_REVIEW, method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteReview(@PathVariable("novel_id") Integer novelID, @RequestHeader("username") String username) {
        FieldErrorDTO deleteReview = reviewService.deleteReview(novelID, username);
        if (deleteReview == null) {
            return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE200), HttpStatus.OK);
        }
        List<FieldErrorDTO> errors = new ArrayList<>();
        errors.add(deleteReview);
        ErrorResponseDTO dto = new ErrorResponseDTO(StatusCode.CODE500, errors);
        return new ResponseEntity<Object>(dto, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * register account for user
     * @param accountDTO
     * @return
     */
    @RequestMapping(value = Const.API_REGISTER, method = RequestMethod.POST)
    public ResponseEntity<Object> register(@RequestBody @Valid AccountDTO accountDTO) {
        List<FieldErrorDTO> errors = checkAccountErrors(accountDTO);
        if (!errors.isEmpty()) {
            return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE400, errors), HttpStatus.BAD_REQUEST);
        }
        FieldErrorDTO responseStr = accountService.register(accountDTO);
        if (responseStr == null) {
            return new ResponseEntity<Object>(new StatusCodeDTO(CODE200), HttpStatus.OK);
        }
        List<FieldErrorDTO> error = new ArrayList<>();
        error.add(responseStr);
        return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE500, error), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * get detailed info from the account for user
     * @param username
     * @return
     */
    @RequestMapping(value = Const.API_USER_DETAIL, method = RequestMethod.GET)
    public ResponseEntity<Object> getUserDetail(@PathVariable("username") String username) {
        if (Utils.isNullOrEmpty(username)) {
            List<FieldErrorDTO> error = new ArrayList<>();
            error.add(new FieldErrorDTO(Const.ERROR_USERNAME, Const.ERROR_USERNAME_BLANK));
            return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE400, error), HttpStatus.BAD_REQUEST);
        }
        UserDetailDTO dto = accountService.getUserDetail(username);
        if (dto != null) {
            ResponseWithStatusCodeDTO reponse = new ResponseWithStatusCodeDTO();
            reponse.setStatus(StatusCode.CODE200.getCode());
            reponse.setContents(dto);
            return new ResponseEntity<Object>(reponse, HttpStatus.OK);
        }
        List<FieldErrorDTO> error = new ArrayList<>();
        error.add(new FieldErrorDTO(Const.ERROR_USERNAME, Const.ERROR_USERNAME_MESSAGE));
        return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE500, error), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * edit account info
     * @param accountDTO
     * @param username
     * @return
     */
    @RequestMapping(value = Const.API_USER_DETAIL, method = RequestMethod.POST)
    public ResponseEntity<Object> editUser(@RequestBody @Valid EditAccountDTO accountDTO, @PathVariable("username") String username) {
        List<FieldErrorDTO> errors = checkEditAccountErrors(accountDTO);
        if (!errors.isEmpty()) {
            return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE400, errors), HttpStatus.BAD_REQUEST);
        }
        FieldErrorDTO responseStr = accountService.editUser(username, accountDTO);
        if (responseStr == null) {
            return new ResponseEntity<Object>(new StatusCodeDTO(CODE200), HttpStatus.OK);
        }
        List<FieldErrorDTO> error = new ArrayList<>();
        error.add(responseStr);
        return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE500, error), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * validate data of review made by user
     * @param userReviewDTO
     * @return
     */
    private List<FieldErrorDTO> checkReviewErrors(UserReviewDTO userReviewDTO) {
        List<FieldErrorDTO> errors = new ArrayList<>();
        if (userReviewDTO.getRating() < Const.MIN_RATING || userReviewDTO.getRating() > Const.MAX_RATING) {
            errors.add(new FieldErrorDTO(Const.ERROR_REVIEW_RATING, Const.ERROR_MESSAGE_RATING));
        }
        if (Utils.isNullOrEmpty(userReviewDTO.getUsername())) {
            errors.add(new FieldErrorDTO(Const.ERROR_REVIEW_USERNAME, Const.ERROR_MESSAGE));
        }
        if (Utils.isNullOrEmpty(userReviewDTO.getReview())) {
            errors.add(new FieldErrorDTO(Const.ERROR_REVIEW_CONTENT, Const.ERROR_MESSAGE));
        }
        return errors;
    }

    /**
     * validate account data of register made by user
     * @param accountDTO
     * @return
     */
    private List<FieldErrorDTO> checkAccountErrors(AccountDTO accountDTO) {
        List<FieldErrorDTO> errors = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat(Const.DATE_PATTERN);
        if (Utils.isNullOrEmpty(accountDTO.getUsername())) {
            errors.add(new FieldErrorDTO(Const.ERROR_USERNAME, Const.ERROR_USERNAME_BLANK));
        }
        if (accountDTO.getUsername().trim().length() < Const.MIN_LENGTH_USERNAME || accountDTO.getUsername().trim().length() > Const.MAX_LENGTH_USERNAME) {
            errors.add(new FieldErrorDTO(Const.ERROR_USERNAME, Const.ERROR_USERNAME_LENGTH));
        }
        if (Utils.isNullOrEmpty(accountDTO.getPassword())) {
            errors.add(new FieldErrorDTO(Const.ERROR_PASSWORD, Const.ERROR_PASSWORD_BLANK));
        }
        if (accountDTO.getPassword().trim().length() < Const.MIN_LENGTH_PASSWORD || accountDTO.getPassword().trim().length() > Const.MAX_LENGTH_PASSWORD) {
            errors.add(new FieldErrorDTO(Const.ERROR_PASSWORD, Const.ERROR_PASSWORD_LENGTH));
        }
        if (Utils.isNullOrEmpty(accountDTO.getSex())) {
            errors.add(new FieldErrorDTO(Const.ERROR_GENDER, Const.ERROR_GENDER_BLANK));
        }
        if (!Utils.isNullOrEmpty(accountDTO.getBirthday())) {
            try {
                Date date = sdf.parse(accountDTO.getBirthday());
                DateTime realDate = new DateTime(date);
                if (realDate.year().get() > Const.MIN_BIRTHDAY_YEAR) {
                    errors.add(new FieldErrorDTO(Const.ERROR_BIRTHDATE, Const.ERROR_BIRTHDAY_YEAR));
                }
            } catch (Exception e) {
                logger.debug("Error birthday: " + e.getMessage());
                errors.add(new FieldErrorDTO(Const.ERROR_BIRTHDATE, Const.ERROR_BIRTHDATE_MESSAGE));
            } finally {
                return errors;
            }
        }
        return errors;
    }

    /**
     * validate account data of edit made by user
     * @param accountDTO
     * @return
     */
    private List<FieldErrorDTO> checkEditAccountErrors(EditAccountDTO accountDTO) {
        List<FieldErrorDTO> errors = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat(Const.DATE_PATTERN);
        if (Utils.isNullOrEmpty(accountDTO.getPassword())) {
            errors.add(new FieldErrorDTO(Const.ERROR_PASSWORD, Const.ERROR_PASSWORD_BLANK));
        }
        if (accountDTO.getPassword().trim().length() < Const.MIN_LENGTH_PASSWORD || accountDTO.getPassword().trim().length() > Const.MAX_LENGTH_PASSWORD) {
            errors.add(new FieldErrorDTO(Const.ERROR_PASSWORD, Const.ERROR_PASSWORD_LENGTH));
        }
        if (Utils.isNullOrEmpty(accountDTO.getSex())) {
            errors.add(new FieldErrorDTO(Const.ERROR_GENDER, Const.ERROR_GENDER_BLANK));
        }
        if (!Utils.isNullOrEmpty(accountDTO.getBirthday())) {
            try {
                Date date = sdf.parse(accountDTO.getBirthday());
                DateTime realDate = new DateTime(date);
                if (realDate.year().get() > Const.MIN_BIRTHDAY_YEAR) {
                    errors.add(new FieldErrorDTO(Const.ERROR_BIRTHDATE, Const.ERROR_BIRTHDAY_YEAR));
                }
            } catch (Exception e) {
                logger.debug("Error birthday: " + e.getMessage());
                errors.add(new FieldErrorDTO(Const.ERROR_BIRTHDATE, Const.ERROR_BIRTHDATE_MESSAGE));
            } finally {
                return errors;
            }
        }
        return errors;
    }

}
