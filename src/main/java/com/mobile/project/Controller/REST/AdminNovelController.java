package com.mobile.project.Controller.REST;

import com.mobile.project.Constant.Const;
import com.mobile.project.Constant.StatusCode;
import com.mobile.project.DTO.*;
import com.mobile.project.DTO.Error.ErrorResponseDTO;
import com.mobile.project.DTO.Error.FieldErrorDTO;
import com.mobile.project.DTO.Response.ResponseWithStatusCodeDTO;
import com.mobile.project.DTO.Response.StatusCodeDTO;
import com.mobile.project.Service.*;
import com.mobile.project.Utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

import static com.mobile.project.Constant.StatusCode.CODE200;

@RestController
@RequestMapping(Const.ROOT_API_FOR_ADMIN)
public class AdminNovelController {

    @Autowired
    NovelService novelService;

    @Autowired
    ChapterService chapterService;

    @Autowired
    AccountService accountService;

    @Autowired
    ReviewService reviewService;

    @Autowired
    AuthorizationService authorizationService;

    /**
     * get list novel
     * @return
     */
    @RequestMapping(value = Const.API_LIST_NOVELS, method = RequestMethod.GET)
    public ResponseEntity<Object> getAll() {
        List<AdminNovelDTO> dtos = novelService.findAllNovelAdmin();
        ResponseWithStatusCodeDTO testDTO = new ResponseWithStatusCodeDTO();
        if (!Utils.isNullOrEmpty(dtos)) {
            testDTO.setStatus(StatusCode.CODE200.getCode());
            testDTO.setContents(dtos);
            return new ResponseEntity<Object>(testDTO, HttpStatus.OK);
        }
        return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE500), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * active, deactivate novel
     * @param id
     * @return
     */
    @RequestMapping(value = Const.API_DEACTIVATE_NOVEL, method = RequestMethod.POST)
    public ResponseEntity<Object> deactivate(@PathVariable("novel_id") int id) {
        try {
            novelService.deactivateNovel(id);
        } catch (Exception e) {
            return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE500), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE200), HttpStatus.OK);
    }

    /**
     * get list genres for adding new novel or editing existed novel
     * @return
     */
    @RequestMapping(value = Const.API_GENRES, method = RequestMethod.GET)
    public ResponseEntity<Object> getGenres() {
        ResponseWithStatusCodeDTO dto = new ResponseWithStatusCodeDTO();
        List<GenreDTO> genres = novelService.getListGenre();
        if (!Utils.isNullOrEmpty(genres)) {
            dto.setStatus(StatusCode.CODE200.getCode());
            dto.setContents(genres);
            return new ResponseEntity<Object>(dto, HttpStatus.OK);
        }
        return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE500), HttpStatus.OK);
    }

    /**
     * add new novel
     * @param addNovelDTO
     * @return
     */
    @RequestMapping(value = Const.API_ADD_NOVEL, method = RequestMethod.POST)
    public ResponseEntity<Object> addNOvel(@RequestBody @Valid AddNovelDTO addNovelDTO) {
        List<FieldErrorDTO> errors = checkNovelError(addNovelDTO);
        if (!errors.isEmpty()) {
            return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE400, errors), HttpStatus.BAD_REQUEST);
        }
        boolean added = novelService.addNovel(addNovelDTO);
        if (!added) {
            return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE400), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE200), HttpStatus.OK);
    }

    /**
     * get detail of a novel
     * @param novelId
     * @return
     */
    @RequestMapping(value = Const.API_NOVEL_DETAIL, method = RequestMethod.GET)
    public ResponseEntity<Object> viewDetail(@PathVariable("novel_id") int novelId) {
        NovelDTO novelDto = novelService.viewNovelDetailAdmin(novelId);
        ResponseWithStatusCodeDTO response = new ResponseWithStatusCodeDTO();
        if (novelDto != null) {
            response.setStatus(StatusCode.CODE200.getCode());
            response.setContents(novelDto);
            return new ResponseEntity<Object>(response, HttpStatus.OK);
        }
        return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE500), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * add a chapter to a novel
     * @param addChapterDTO
     * @return
     */
    @RequestMapping(value = Const.API_ADD_CHAPTER, method = RequestMethod.POST)
    public ResponseEntity<Object> addChapter(@RequestBody @Valid AddChapterDTO addChapterDTO) {
        List<FieldErrorDTO> errors = checkChapterError(addChapterDTO);
        if (!errors.isEmpty()) {
            return new ResponseEntity<>(new ErrorResponseDTO(StatusCode.CODE400, errors), HttpStatus.BAD_REQUEST);
        }
        FieldErrorDTO added = chapterService.addChapter(addChapterDTO);
        if (added == null) {
            return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE200), HttpStatus.OK);
        }
        List<FieldErrorDTO> serverError = new ArrayList<>();
        serverError.add(added);
        return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE500, serverError), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * get list chapters of a novel
     * @param novelId
     * @return
     */
    @RequestMapping(value = Const.API_LIST_CHAPTERS, method = RequestMethod.GET)
    public ResponseEntity<Object> getListChapters(@PathVariable("novel_id") int novelId) {
        AdminChapterListDTO dto = chapterService.getListChaptersAdmin(novelId);
        if (dto == null) {
            return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE500), HttpStatus.OK);
        }
        ResponseWithStatusCodeDTO response = new ResponseWithStatusCodeDTO();
        response.setStatus(CODE200.getCode());
        response.setContents(dto);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    /**
     * get detail of a chapter on edit modal for admin
     * @param chapterId
     * @return
     */
    @RequestMapping(value = Const.API_CHAPTER_DETAIL, method = RequestMethod.GET)
    public ResponseEntity<Object> getChapterDetail(@PathVariable("chapter_id") int chapterId) {
        ChapterDTO dto = chapterService.getOneChapter(chapterId);
        if (dto != null) {
            ResponseWithStatusCodeDTO response = new ResponseWithStatusCodeDTO();
            response.setStatus(CODE200.getCode());
            response.setContents(dto);
            return new ResponseEntity<Object>(response, HttpStatus.OK);
        }
        return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE500), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * change password admin, with confirm old pasword correct first
     * @param username
     * @param changePassDTO
     * @return
     */
    @RequestMapping(value = Const.API_USER_DETAIL, method = RequestMethod.POST)
    public ResponseEntity<Object> getUserDetail(@PathVariable("username") String username, @RequestBody @Valid ChangePassDTO changePassDTO) {
        List<FieldErrorDTO> error = new ArrayList<>();
        if (changePassDTO.getNewPassword().trim().length() < Const.MIN_LENGTH_PASSWORD || changePassDTO.getNewPassword().trim().length() > Const.MAX_LENGTH_PASSWORD) {
            error.add(new FieldErrorDTO(Const.ERROR_NEW_PASSWORD, Const.ERROR_PASSWORD_LENGTH));
        }
        if (!error.isEmpty()) {
            return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE400, error), HttpStatus.BAD_REQUEST);
        }
        boolean exist = authorizationService.checkLogin(username, changePassDTO.getOldPassword(), Const.ROLE_ADMIN);
        if (!exist) {
            error.add(new FieldErrorDTO(Const.ERROR_OLD_PASSWORD, Const.ERROR_USERNAME_PASSWORD));
            return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE401, error), HttpStatus.UNAUTHORIZED);
        } else {
            boolean changed = accountService.changePassword(username, changePassDTO.getNewPassword());
            if (changed) {
                return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE200), HttpStatus.OK);
            }
        }
        return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE500), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * edit a chapter
     * @param chaperId
     * @param editChapterDTO
     * @return
     */
    @RequestMapping(value = Const.API_CHAPTER_DETAIL, method = RequestMethod.POST)
    public ResponseEntity<Object> editChapter(@PathVariable("chapter_id") Integer chaperId, @RequestBody @Valid EditChapterDTO editChapterDTO) {
        List<FieldErrorDTO> errors = checkEditChapterError(editChapterDTO);
        if (!errors.isEmpty()) {
            return new ResponseEntity<>(new ErrorResponseDTO(StatusCode.CODE400, errors), HttpStatus.BAD_REQUEST);
        }
        FieldErrorDTO added = chapterService.editChapter(chaperId, editChapterDTO);
        if (added == null) {
            return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE200), HttpStatus.OK);
        }
        List<FieldErrorDTO> serverError = new ArrayList<>();
        serverError.add(added);
        return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE500, serverError), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * get list user
     * @return
     */
    @RequestMapping(value = Const.API_LIST_USER, method = RequestMethod.GET)
    public ResponseEntity<Object> getListUsers() {
        List<AccountForAdminDTO> dtos = accountService.getListUsers();
        ResponseWithStatusCodeDTO response = new ResponseWithStatusCodeDTO();
        response.setStatus(CODE200.getCode());
        response.setContents(dtos);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    /**
     * get list reviews for admin
     * @return
     */
    @RequestMapping(value = Const.API_LIST_REVIEWS_ADMIN, method = RequestMethod.GET)
    public ResponseEntity<Object> getListReviews() {
        List<ReviewAdminDTO> dtos = reviewService.getListReivews();
        ResponseWithStatusCodeDTO response = new ResponseWithStatusCodeDTO();
        response.setStatus(CODE200.getCode());
        response.setContents(dtos);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    /**
     * delete reviews
     * @param reviewId
     * @return
     */
    @RequestMapping(value = Const.API_DELETE_REVIEW_ADMIN, method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteReview(@PathVariable("review_id") Integer reviewId) {
        FieldErrorDTO deleteReview = reviewService.deleteReviewForAdmin(reviewId);
        if (deleteReview == null) {
            return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE200), HttpStatus.OK);
        }
        List<FieldErrorDTO> errors = new ArrayList<>();
        errors.add(deleteReview);
        ErrorResponseDTO dto = new ErrorResponseDTO(StatusCode.CODE500, errors);
        return new ResponseEntity<Object>(dto, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * ban and unban a user
     * @param username
     * @param banTime
     * @return
     */
    @RequestMapping(value = Const.API_DEACTIVE_USER, method = RequestMethod.POST)
    public ResponseEntity<Object> deactivateUser(@PathVariable("username") String username, @RequestBody(required = false) BanTimeDTO banTime) {
        FieldErrorDTO deactivateUser = null;
        if(banTime == null) {
            deactivateUser = accountService.deactivateUser(username, null);
        } else {
            deactivateUser = accountService.deactivateUser(username, banTime.getBantime());
        }
        if (deactivateUser == null) {
            return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE200), HttpStatus.OK);
        }
        List<FieldErrorDTO> errors = new ArrayList<>();
        errors.add(deactivateUser);
        ErrorResponseDTO dto = new ErrorResponseDTO(StatusCode.CODE500, errors);
        return new ResponseEntity<Object>(dto, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * edit a novel
     * @param novelId
     * @param editNovelDTO
     * @return
     */
    @RequestMapping(value = Const.API_NOVEL_DETAIL, method = RequestMethod.POST)
    public ResponseEntity<Object> editNOvel(@PathVariable("novel_id") Integer novelId, @RequestBody @Valid EditNovelDTO editNovelDTO) {
        List<FieldErrorDTO> errors = checkEditNovelError(editNovelDTO);
        if (!errors.isEmpty()) {
            return new ResponseEntity<Object>(new ErrorResponseDTO(StatusCode.CODE400, errors), HttpStatus.BAD_REQUEST);
        }
        FieldErrorDTO edited = novelService.editNovel(novelId, editNovelDTO);
        if (edited == null) {
            return new ResponseEntity<Object>(new StatusCodeDTO(StatusCode.CODE200), HttpStatus.OK);
        }
        errors = new ArrayList<>();
        errors.add(edited);
        ErrorResponseDTO dto = new ErrorResponseDTO(StatusCode.CODE500, errors);
        return new ResponseEntity<Object>(dto, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * validate data send from client when adding new novel
     * @param addNovelDTO
     * @return
     */
    private List<FieldErrorDTO> checkNovelError(AddNovelDTO addNovelDTO) {
        List<FieldErrorDTO> errors = new ArrayList<>();
        if (Utils.isNullOrEmpty(addNovelDTO.getNovelName())) {
            errors.add(new FieldErrorDTO(Const.ERROR_NOVEL_NAME, Const.ERROR_MESSAGE));
        }
        if (Utils.isNullOrEmpty(addNovelDTO.getAuthor())) {
            errors.add(new FieldErrorDTO(Const.ERROR_NOVEL_AUTHOR, Const.ERROR_MESSAGE));
        }
        if (Utils.isNullOrEmpty(addNovelDTO.getGenreIds())) {
            errors.add(new FieldErrorDTO(Const.ERROR_NOVEL_GENRE, Const.ERROR_MESSAGE_GENRE));
        }
        return errors;
    }

    /**
     * validate data send from client when editing an existed novel
     * @param editNovelDTO
     * @return
     */
    private List<FieldErrorDTO> checkEditNovelError(EditNovelDTO editNovelDTO) {
        List<FieldErrorDTO> errors = new ArrayList<>();
        errors = checkNovelError(editNovelDTO);
        if (Utils.isNullOrEmpty(editNovelDTO.getStatusTranslated())) {
            errors.add(new FieldErrorDTO(Const.ERROR_NOVEL_STATUS_TRANSLATED, Const.ERROR_MESSAGE_STATUS_TRANSLATED));
        } else if (!(editNovelDTO.getStatusTranslated().equals(Const.ONGOING) || editNovelDTO.getStatusTranslated().equals(Const.COMPLETE))) {
            errors.add(new FieldErrorDTO(Const.ERROR_NOVEL_STATUS_TRANSLATED, Const.ERROR_MESSAGE_STATUS_TRANSLATED_FORMAT));
        }
        return errors;
    }

    /**
     * validate data sent from client when adding new chapter
     * @param addChapterDTO
     * @return
     */
    private List<FieldErrorDTO> checkChapterError(AddChapterDTO addChapterDTO) {
        List<FieldErrorDTO> errors = new ArrayList<>();
        if (addChapterDTO.getNovelId() == null) {
            errors.add(new FieldErrorDTO(Const.ERROR_NOVEL_ID, Const.ERROR_MESSAGE_NOVEL_ID));
        }
        if (Utils.isNullOrEmpty(addChapterDTO.getTitle())) {
            errors.add(new FieldErrorDTO(Const.ERROR_CHAPTER_TITLE, Const.ERROR_MESSAGE));
        }
        if (Utils.isNullOrEmpty(addChapterDTO.getContent())) {
            errors.add(new FieldErrorDTO(Const.ERROR_CHAPTER_CONTENT, Const.ERROR_MESSAGE));
        }
        return errors;
    }

    /**
     * validate data sent from client when edit an existed chapter
     * @param editChapterDTO
     * @return
     */
    private List<FieldErrorDTO> checkEditChapterError(EditChapterDTO editChapterDTO) {
        List<FieldErrorDTO> errors = new ArrayList<>();
        if (Utils.isNullOrEmpty(editChapterDTO.getTitle())) {
            errors.add(new FieldErrorDTO(Const.ERROR_CHAPTER_TITLE, Const.ERROR_MESSAGE));
        }
        if (Utils.isNullOrEmpty(editChapterDTO.getContent())) {
            errors.add(new FieldErrorDTO(Const.ERROR_CHAPTER_CONTENT, Const.ERROR_MESSAGE));
        }
        return errors;
    }
}
