package com.mobile.project.Controller.MVC;

import com.mobile.project.Constant.Const;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {
    //Controller to switch view on web

    //home page/novel list page
    @RequestMapping(value = {Const.ADMIN_LIST_NOVEL, "/"})
    public String toAdmin(Model model) {
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return "list_novel";
    }

    //login page
    @RequestMapping(Const.ADMIN_LOGIN)
    public String toLogin() {
        return "login";
    }

    //add novel page
    @RequestMapping(Const.ADMIN_ADD_NOVEL)
    public String toAddNovel(Model model) {
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return "addNovel";
    }

    //access denied page for user attempt to login on admin page
    @RequestMapping(Const.ACCESS_DENIED)
    public String deniedAccess() {
        return "accessDenied";
    }

    //list chapters page
    @RequestMapping(Const.ADMIN_LIST_CHAPTER)
    public String toChaper(@PathVariable("novel_id") Integer id, Model model) {
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        model.addAttribute("novel_id", id);
        return "list_chapter";
    }

    //add chapter page
    @RequestMapping(Const.ADMIN_ADD_CHAPTER)
    public String toAddChaper(@PathVariable("novel_id") Integer id, Model model) {
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        model.addAttribute("novel_id", id);
        return "addChapter";
    }

    //novel detail page
    @RequestMapping(Const.ADMIN_DETAIL_NOVEL)
    public String toDetailNovel(@PathVariable("novel_id") Integer id, Model model) {
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        model.addAttribute("novel_id", id);
        return "novelDetail";
    }

    //list user page
    @RequestMapping(Const.ADMIN_LIST_USER)
    public String toUser(Model model) {
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return "list_user";
    }

    //list review page
    @RequestMapping(Const.ADMIN_LIST_REVIEWS)
    public String toReviews(Model model) {
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return "list_reviews";
    }

    //change password for admin page
    @RequestMapping(Const.ADMIN_DETAIL)
    public String toDetails(@PathVariable("username") String username, Model model) {
        model.addAttribute("user", ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return "admin";
    }
}
