package com.mobile.project.Controller.ErrorHandling;

import com.mobile.project.Constant.Const;
import com.mobile.project.Constant.StatusCode;
import com.mobile.project.DTO.Error.ErrorResponseDTO;
import com.mobile.project.DTO.Error.FieldErrorDTO;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import java.util.ArrayList;
import java.util.List;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class CustomErrorHandling extends ResponseEntityExceptionHandler {

    //handler for request sent in wrong format
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<FieldErrorDTO> errors = new ArrayList<>();
        errors.add(new FieldErrorDTO(Const.ERROR_REQUEST_BODY, ex.toString()));
        ErrorResponseDTO dto = new ErrorResponseDTO(StatusCode.CODE400, errors);
        return new ResponseEntity<Object>(dto, HttpStatus.BAD_REQUEST);
    }

    //handler for path variable in wrong format
    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<FieldErrorDTO> errors = new ArrayList<>();
        errors.add(new FieldErrorDTO(Const.ERROR_URL_WRONG_TYPE, ex.toString()));
        ErrorResponseDTO dto = new ErrorResponseDTO(StatusCode.CODE400, errors);
        return new ResponseEntity<Object>(dto, HttpStatus.BAD_REQUEST);
    }

    //handler for not found
    @Override
    protected ResponseEntity<Object> handleNoSuchRequestHandlingMethod(NoSuchRequestHandlingMethodException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<FieldErrorDTO> errors = new ArrayList<>();
        errors.add(new FieldErrorDTO(Const.ERROR_REQUEST_NOT_FOUND, ex.toString()));
        ErrorResponseDTO dto = new ErrorResponseDTO(StatusCode.CODE400, errors);
        return new ResponseEntity<Object>(dto, HttpStatus.BAD_REQUEST);
    }

    //handler for not found
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<FieldErrorDTO> errors = new ArrayList<>();
        errors.add(new FieldErrorDTO(Const.ERROR_REQUEST_NOT_FOUND, ex.toString()));
        ErrorResponseDTO dto = new ErrorResponseDTO(StatusCode.CODE400, errors);
        return new ResponseEntity<Object>(dto, HttpStatus.BAD_REQUEST);
    }

}