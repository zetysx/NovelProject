package com.mobile.project.Repository;

import com.mobile.project.Enity.Novel;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NovelRepostitory extends JpaRepository<Novel, Integer> {
    public Novel findByNovelIdAndActive(int novelId, boolean active, Sort sort);
    public List<Novel> findByActive(boolean active, Sort sort);
    public List<Novel> findTop5ByActiveAndNovelNameContaining(boolean active, String search, Sort sort);
}
