package com.mobile.project.Repository;

import com.mobile.project.DTO.ChapterDetailDTO;
import com.mobile.project.DTO.MiniChapterDTO;
import com.mobile.project.Enity.Chapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChapterRepository extends JpaRepository<Chapter, Integer> {
    @Query("Select new com.mobile.project.DTO.MiniChapterDTO(c.chapterId, c.chapterTitle) from Chapter c where c.novelId=:novelId order by releaseDate asc")
    public List<MiniChapterDTO> getListChapters(@Param("novelId") int novelId);
    public int countByNovelId(int novelId);
    @Query("Select new com.mobile.project.DTO.ChapterDetailDTO(c.chapterId, c.chapterTitle, substring(c.chapterContent,1,1000)) from Chapter c where c.novelId=:novelId order by releaseDate asc")
    public List<ChapterDetailDTO> getListChapterAdmin(@Param("novelId") int novelId);
}
