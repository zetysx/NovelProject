package com.mobile.project.Repository;

import com.mobile.project.Enity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    public User findByUsernameAndPasswordAndEnabledAndRole(String username, String password, boolean enabled, String role);
    public User findByUsernameAndEnabledAndRole(String username, boolean enabled, String role);
    public User findByUsernameAndEnabled(String username, boolean enabled);
    public User findByUsername(String username);

    @Query("Select c.avatar from User c where c.username=:username")
    public String findAvatarByUsername(@Param("username") String username);

    public List<User> findAllByRole(String role);
}
