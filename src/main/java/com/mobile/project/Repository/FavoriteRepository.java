package com.mobile.project.Repository;

import com.mobile.project.Enity.Favorite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavoriteRepository extends JpaRepository<Favorite, Integer> {
    public Favorite findByNovelIdAndUsername(int novelId, String username);
    public List<Favorite> findByUsername(String username);
}
