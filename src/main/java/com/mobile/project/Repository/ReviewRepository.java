package com.mobile.project.Repository;

import com.mobile.project.Enity.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Integer> {

    public Review findByNovelIdAndUsername(int novelId, String username);

    @Query("Select c.rating from Review c where c.novelId=:novel_id")
    public List<Integer> getRatings(@Param("novel_id") int novelId);

    public List<Review> findByNovelId(int novelId);
}
