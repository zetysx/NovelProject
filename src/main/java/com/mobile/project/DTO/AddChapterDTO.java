package com.mobile.project.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AddChapterDTO {
    private Integer novelId;
    private String title;
    private String content;

    @JsonCreator
    public AddChapterDTO(@JsonProperty("novelID") Integer novelId,
                         @JsonProperty("title") String title,
                         @JsonProperty("content") String content) {
        this.novelId = novelId;
        this.title = title;
        this.content = content;
    }

    public Integer getNovelId() {
        return novelId;
    }

    public void setNovelId(Integer novelId) {
        this.novelId = novelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
