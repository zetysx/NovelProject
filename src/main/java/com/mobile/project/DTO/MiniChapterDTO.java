package com.mobile.project.DTO;


public class MiniChapterDTO {

  private int chapterId;
  private String chapterTitle;

  public MiniChapterDTO(int chapterId, String chapterTitle) {
    this.chapterId = chapterId;
    this.chapterTitle = chapterTitle;
  }

  public int getChapterId() {
    return chapterId;
  }

  public void setChapterId(int chapterId) {
    this.chapterId = chapterId;
  }


  public String getChapterTitle() {
    return chapterTitle;
  }

  public void setChapterTitle(String chapterTitle) {
    this.chapterTitle = chapterTitle;
  }

}
