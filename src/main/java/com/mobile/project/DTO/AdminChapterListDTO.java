package com.mobile.project.DTO;

import java.util.List;

public class AdminChapterListDTO {
    private String cover;
    private String novemName;
    private String author;
    private List<ChapterDetailDTO> chapters;

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getNovemName() {
        return novemName;
    }

    public void setNovemName(String novemName) {
        this.novemName = novemName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<ChapterDetailDTO> getChapters() {
        return chapters;
    }

    public void setChapters(List<ChapterDetailDTO> chapters) {
        this.chapters = chapters;
    }
}
