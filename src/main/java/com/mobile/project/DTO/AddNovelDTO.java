package com.mobile.project.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class AddNovelDTO {
    protected String novelName;
    protected String image;
    protected String author;
    protected int year;
    protected String description;
    protected List<Integer> genreIds;

    public AddNovelDTO() {}

    @JsonCreator
    public AddNovelDTO(@JsonProperty("novelName") String novelName,
                       @JsonProperty("image") String image,
                       @JsonProperty("author") String author,
                       @JsonProperty("year") int year,
                       @JsonProperty("genreIds") List<Integer> genreIds,
                       @JsonProperty("description") String description ) {
        this.novelName = novelName;
        this.image = image;
        this.author = author;
        this.year = year;
        this.genreIds = genreIds;
        this.description = description;
    }

    public String getNovelName() {
        return novelName;
    }

    public void setNovelName(String novelName) {
        this.novelName = novelName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Integer> getGenreIds() {
        return genreIds;
    }

    public void setGenreIds(List<Integer> genreIds) {
        this.genreIds = genreIds;
    }
}
