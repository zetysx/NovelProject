package com.mobile.project.DTO.Response;

import com.mobile.project.Constant.StatusCode;

public class StatusCodeDTO {
    private int code;
    private String message;

    public StatusCodeDTO(){}

    public StatusCodeDTO(StatusCode statusCode) {
        this.code = statusCode.getCode();
        this.message = statusCode.getMassage();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
