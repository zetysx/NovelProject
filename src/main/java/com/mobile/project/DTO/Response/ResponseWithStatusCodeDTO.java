package com.mobile.project.DTO.Response;

public class ResponseWithStatusCodeDTO {
	private int status;
	private Object novels;
	
	public ResponseWithStatusCodeDTO() {
		
	}

	

	public int getStatus() {
		return status;
	}



	public void setStatus(int status) {
		this.status = status;
	}



	public Object getContents() {
		return novels;
	}

	public void setContents(Object novels) {
		this.novels = novels;
	}
	
	
}
