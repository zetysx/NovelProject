package com.mobile.project.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserReviewDTO {
    private int novelId;
    private String username;
    private int rating;
    private String review;

    @JsonCreator
    public UserReviewDTO(@JsonProperty("novel_id") int novelId,@JsonProperty("username") String username,@JsonProperty("rating") int rating,@JsonProperty("review") String review) {
        this.novelId = novelId;
        this.username = username;
        this.rating = rating;
        this.review = review;
    }

    public int getNovelId() {
        return novelId;
    }

    public void setNovelId(int novelId) {
        this.novelId = novelId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
