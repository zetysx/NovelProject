package com.mobile.project.DTO;

public class NovelWIthFavoriteDTO extends NovelDTO {
    private boolean isFavorite;
    private boolean isReviewed;
    private Integer ratingForUser;
    private String reviewUser;

    public NovelWIthFavoriteDTO(NovelDTO dto) {
        this.novelId = dto.getNovelId();
        this.novelName = dto.getNovelName();
        this.cover = dto.getCover();
        this.description = dto.getDescription();
        this.author = dto.getAuthor();
        this.year = dto.getYear();
        this.statusTranslate = dto.getStatusTranslate();
        this.ratings = dto.getRatings();
        this.views = dto.getViews();
        this.active = dto.isActive();
        this.lastUpdated = dto.getLastUpdated();
        this.genres = dto.getGenres();
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public boolean isReviewed() {
        return isReviewed;
    }

    public void setReviewed(boolean reviewed) {
        isReviewed = reviewed;
    }

    public Integer getRatingForUser() {
        return ratingForUser;
    }

    public void setRatingForUser(Integer ratingForUser) {
        this.ratingForUser = ratingForUser;
    }

    public String getReviewUser() {
        return reviewUser;
    }

    public void setReviewUser(String reviewUser) {
        this.reviewUser = reviewUser;
    }
}
