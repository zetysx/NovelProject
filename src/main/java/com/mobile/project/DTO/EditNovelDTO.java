package com.mobile.project.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class EditNovelDTO extends AddNovelDTO {
    private String statusTranslated;

    @JsonCreator
    public EditNovelDTO(@JsonProperty("novelName") String novelName,
                        @JsonProperty("image") String image,
                        @JsonProperty("author") String author,
                        @JsonProperty("statusTranslate") String statusTranslated,
                        @JsonProperty("year") int year,
                        @JsonProperty("genreIds") List<Integer> genreIds,
                        @JsonProperty("description") String description) {
        this.novelName = novelName;
        this.image = image;
        this.author = author;
        this.year = year;
        this.genreIds = genreIds;
        this.description = description;
        this.statusTranslated = statusTranslated;
    }

    public String getStatusTranslated() {
        return statusTranslated;
    }

    public void setStatusTranslated(String statusTranslated) {
        this.statusTranslated = statusTranslated;
    }
}
