package com.mobile.project.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BanTimeDTO {
    private Integer bantime;

    @JsonCreator
    public BanTimeDTO(@JsonProperty("banTime") Integer bantime) {
        this.bantime = bantime;
    }

    public Integer getBantime() {
        return bantime;
    }

    public void setBantime(Integer bantime) {
        this.bantime = bantime;
    }
}
