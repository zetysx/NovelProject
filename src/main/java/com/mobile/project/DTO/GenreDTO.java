package com.mobile.project.DTO;

public class GenreDTO {
    private int genreId;
    private String genreName;

    public GenreDTO(int genreID, String genreName) {
        this.genreId = genreID;
        this.genreName = genreName;
    }

    public GenreDTO() {

    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }
}
