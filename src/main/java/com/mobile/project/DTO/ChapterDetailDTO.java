package com.mobile.project.DTO;

public class ChapterDetailDTO {
    private Integer chapterId;
    private String chapterTitle;
    private String content;

    public ChapterDetailDTO(Integer chapterId, String chapterTitle, String content) {
        this.chapterId = chapterId;
        this.chapterTitle = chapterTitle;
        this.content = content;
    }

    public Integer getChapterId() {
        return chapterId;
    }

    public void setChapterId(Integer chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterTitle() {
        return chapterTitle;
    }

    public void setChapterTitle(String chapterTitle) {
        this.chapterTitle = chapterTitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
