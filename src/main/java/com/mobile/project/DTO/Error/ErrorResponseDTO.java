package com.mobile.project.DTO.Error;

import com.mobile.project.Constant.StatusCode;
import com.mobile.project.DTO.Response.StatusCodeDTO;

import java.util.ArrayList;
import java.util.List;

public class ErrorResponseDTO extends StatusCodeDTO {

    private List<FieldErrorDTO> errors = new ArrayList<>();

    public ErrorResponseDTO(StatusCode statusCode, List<FieldErrorDTO> errors) {
        super(statusCode);
        this.errors = errors;
    }


    public List<FieldErrorDTO> getErrors() {
        return errors;
    }

    public void setErrors(List<FieldErrorDTO> errors) {
        this.errors = errors;
    }
}
