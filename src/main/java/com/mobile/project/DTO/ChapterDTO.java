package com.mobile.project.DTO;


public class ChapterDTO {

  private int chapterId;
  private int novelId;
  private String chapterTitle;
  private String chapterContent;
  private String releaseDate;


  public int getChapterId() {
    return chapterId;
  }

  public void setChapterId(int chapterId) {
    this.chapterId = chapterId;
  }


  public int getNovelId() {
    return novelId;
  }

  public void setNovelId(int novelId) {
    this.novelId = novelId;
  }


  public String getChapterTitle() {
    return chapterTitle;
  }

  public void setChapterTitle(String chapterTitle) {
    this.chapterTitle = chapterTitle;
  }


  public String getChapterContent() {
    return chapterContent;
  }

  public void setChapterContent(String chapterContent) {
    this.chapterContent = chapterContent;
  }


  public String getReleaseDate() {
    return releaseDate;
  }

  public void setReleaseDate(String releaseDate) {
    this.releaseDate = releaseDate;
  }

}
