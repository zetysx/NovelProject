package com.mobile.project.DTO;

public class GenreUserDTO extends GenreDTO {
    private String cover;

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}
