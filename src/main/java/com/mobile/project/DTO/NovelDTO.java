package com.mobile.project.DTO;


import java.util.ArrayList;
import java.util.List;


public class NovelDTO {

  protected int novelId;
  protected String novelName;
  protected String cover;
  protected String description;
  protected String author;
  protected int year;
  protected String statusTranslate;
  protected double ratings;
  protected int views;
  protected boolean active;
  protected String lastUpdated;
  protected List<GenreDTO> genres = new ArrayList<>();

  public int getNovelId() {
    return novelId;
  }

  public void setNovelId(int novelId) {
    this.novelId = novelId;
  }


  public String getNovelName() {
    return novelName;
  }

  public void setNovelName(String novelName) {
    this.novelName = novelName;
  }


  public String getCover() {
    return cover;
  }

  public void setCover(String cover) {
    this.cover = cover;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }


  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }


  public String getStatusTranslate() {
    return statusTranslate;
  }

  public void setStatusTranslate(String statusTranslate) {
    this.statusTranslate = statusTranslate;
  }


  public double getRatings() {
    return ratings;
  }

  public void setRatings(double ratings) {
    this.ratings = ratings;
  }


  public int getViews() {
    return views;
  }

  public void setViews(int views) {
    this.views = views;
  }


  public void setActive(boolean active) {
    this.active = active;
  }


  public String getLastUpdated() {
    return lastUpdated;
  }

  public void setLastUpdated(String lastUpdated) {
    this.lastUpdated = lastUpdated;
  }

  public boolean isActive() {
    return active;
  }

  public List<GenreDTO> getGenres() {
    return genres;
  }

  public void setGenres(List<GenreDTO> genres) {
    this.genres = genres;
  }
}
