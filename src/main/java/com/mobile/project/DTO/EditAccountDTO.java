package com.mobile.project.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class EditAccountDTO {
    private String password;
    private String fullname;
    private String sex;
    private String birthday;
    private String avatar;

    @JsonCreator
    public EditAccountDTO(@JsonProperty("password") String password,
                      @JsonProperty("fullname") String fullname,
                      @JsonProperty("gender")   String sex,
                      @JsonProperty("birthday") String birthday,
                      @JsonProperty("avatar")   String avatar) {
        this.password = password;
        this.fullname = fullname;
        this.sex = sex;
        this.birthday = birthday;
        this.avatar = avatar;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
