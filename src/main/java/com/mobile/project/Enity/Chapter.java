package com.mobile.project.Enity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "Chapter")
public class Chapter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "chapter_id", nullable = false)
    private int chapterId;

    @Column(name = "novel_id", nullable = false)
    private int novelId;

    @Column(name = "chapter_title", nullable = false, length = 30)
    private String chapterTitle;

    @Column(name = "chapter_content", nullable = false)
    private String chapterContent;

    @Column(name = "release_date", nullable = false)
    private Timestamp releaseDate;

    public void setNovelId(int novelId) {
        this.novelId = novelId;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public int getNovelId() {
        return novelId;
    }

    public String getChapterTitle() {
        return chapterTitle;
    }

    public void setChapterTitle(String chapterTitle) {
        this.chapterTitle = chapterTitle;
    }

    public String getChapterContent() {
        return chapterContent;
    }

    public void setChapterContent(String chapterContent) {
        this.chapterContent = chapterContent;
    }

    public Timestamp getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Timestamp releaseDate) {
        this.releaseDate = releaseDate;
    }
}
