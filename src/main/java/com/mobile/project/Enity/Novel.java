package com.mobile.project.Enity;

import com.mobile.project.DTO.GenreDTO;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Novel")
public class Novel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "novel_id", nullable = false)
    private int novelId;

    @Column(name = "novel_name", nullable = false, length = 50)
    private String novelName;

    @Column(name = "cover", nullable = true)
    private String cover;

    @Column(name = "description", nullable = true)
    private String description;

    @Column(name = "author", nullable = false, length = 50)
    private String author;

    @Column(name = "year", nullable = true)
    private Integer year;

    @Column(name = "status_translate", nullable = true)
    private Boolean statusTranslate;

    @Column(name = "ratings", nullable = true, precision = 0)
    private Double ratings;

    @Column(name = "views", nullable = true)
    private Integer views;

    @Column(name = "last_updated")
    private Timestamp lastUpdated;

    @Column(name = "active", nullable = false)
    private boolean active;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "GenreNovel",
            joinColumns = { @JoinColumn(name = "novel_id", referencedColumnName = "novel_id") },
            inverseJoinColumns = { @JoinColumn(name = "genre_id", referencedColumnName = "genre_id") })
    private List<Genre> genres = new ArrayList<>();

    public int getNovelId() {
        return novelId;
    }

    public void setNovelId(int novelId) {
        this.novelId = novelId;
    }

    public String getNovelName() {
        return novelName;
    }

    public void setNovelName(String novelName) {
        this.novelName = novelName;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Boolean getStatusTranslate() {
        return statusTranslate;
    }

    public void setStatusTranslate(Boolean statusTranslate) {
        this.statusTranslate = statusTranslate;
    }

    public Double getRatings() {
        return ratings;
    }

    public void setRatings(Double ratings) {
        this.ratings = ratings;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Timestamp getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Timestamp lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public List<String> getGenreNames() {
        List<String> genreNames = new ArrayList<>();
        if(!genres.isEmpty()) {
            for (Genre genre: genres) {
                genreNames.add(genre.getGenre());
            }
        }
        return genreNames;
    }

    public List<GenreDTO> getGenreDTO() {
        List<GenreDTO> dtos = new ArrayList<>();
        GenreDTO dto = null;
        if(!genres.isEmpty()) {
            for (Genre genre : genres) {
                dto = new GenreDTO();
                dto.setGenreId(genre.getGenreId());
                dto.setGenreName(genre.getGenre());
                dtos.add(dto);
            }
        }
        return dtos;
    }
}
