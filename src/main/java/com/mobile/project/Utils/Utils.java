package com.mobile.project.Utils;

import java.util.List;

public class Utils {

    //check null or empty a list of object
    public static boolean isNullOrEmpty(List<?> list) {
        if (list == null) {
            return true;
        } else if (list.isEmpty()) {
            return true;
        }
        return false;
    }

    //check null or empty a string
    public static boolean isNullOrEmpty(String str) {
        if (str == null) {
            return true;
        } else {
            str = str.trim();
            if (str.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    //get average of a list of integer
    public static Double getAverage(List<Integer> lsints) {
        int sum = 0;
        for (Integer n : lsints) {
            sum += n;
        }
        return round(sum * 1.0 / lsints.size(), 1);
    }

    //round a double to number of precision
    private static double round(double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    //add suffix to imgur image url for imgur to resize
    public static String resizeImage(String url, String s) {
        int index = url.lastIndexOf(".");
        String resizedUrl = url.substring(0, index) + s + url.substring(index, url.length());
        return resizedUrl;

    }

}
