package com.mobile.project.Service;

import com.mobile.project.DTO.AccountDTO;
import com.mobile.project.DTO.AccountForAdminDTO;
import com.mobile.project.DTO.EditAccountDTO;
import com.mobile.project.DTO.Error.FieldErrorDTO;
import com.mobile.project.DTO.UserDetailDTO;

import java.util.List;

public interface AccountService {
    public FieldErrorDTO register(AccountDTO accountDTO);
    public UserDetailDTO getUserDetail(String username);
    public FieldErrorDTO editUser(String username, EditAccountDTO accountDTO);
    public List<AccountForAdminDTO> getListUsers();
    public FieldErrorDTO deactivateUser(String username, Integer banTime);
    public boolean changePassword(String username, String password);
}
