package com.mobile.project.Service;

public interface AuthorizationService {
    public boolean checkLogin(String username, String password, String role);
    public String checkBanned(String username);
    public boolean checkValid(String username, String password);
}
