package com.mobile.project.Service;

import com.mobile.project.DTO.*;
import com.mobile.project.DTO.Error.FieldErrorDTO;

import java.util.List;

public interface ChapterService {
    public List<MiniChapterDTO> getListChapters(int novelId);

    public ChapterDTO getOneChapter(int chapterID);

    public FieldErrorDTO addChapter(AddChapterDTO addChapterDTO);

    public AdminChapterListDTO getListChaptersAdmin(int novelId);

    public FieldErrorDTO editChapter(Integer chapterId, EditChapterDTO editChapterDTO);
}
