package com.mobile.project.Service;

import com.mobile.project.DTO.Error.FieldErrorDTO;
import com.mobile.project.DTO.RatingForNovelDetailDTO;
import com.mobile.project.DTO.ReviewAdminDTO;
import com.mobile.project.DTO.ReviewNovelDTO;
import com.mobile.project.DTO.UserReviewDTO;

import java.util.List;

public interface ReviewService {
    public FieldErrorDTO addNewReview(UserReviewDTO userReviewDTO);

    public RatingForNovelDetailDTO isReviewed(int novelId, String username);

    public List<ReviewNovelDTO> getReviewsNovel(int novelId);

    public FieldErrorDTO deleteReview(int novelId, String username);

    public List<ReviewAdminDTO> getListReivews();

    public FieldErrorDTO deleteReviewForAdmin(Integer reviewId);
}
