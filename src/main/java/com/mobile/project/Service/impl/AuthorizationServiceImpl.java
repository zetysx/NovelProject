package com.mobile.project.Service.impl;

import com.mobile.project.Constant.Const;
import com.mobile.project.Enity.Banned;
import com.mobile.project.Enity.User;
import com.mobile.project.Repository.BannedRepository;
import com.mobile.project.Repository.UserRepository;
import com.mobile.project.Service.AuthorizationService;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthorizationServiceImpl implements AuthorizationService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BannedRepository bannedRepository;

    @Autowired
    PasswordEncoder encoder;

    /**
     * check login, used to authorized user and for admin to change password
     * @param username
     * @param password
     * @param role
     * @return
     */
    @Override
    public boolean checkLogin(String username, String password, String role) {
        User user = userRepository.findByUsernameAndEnabledAndRole(username, true, role);
        if (user != null) {
            if (encoder.matches(password, user.getPassword())) {
                return true;
            }
        }
        return false;
    }

    /**
     * check if user is banned, if banned return the time the ban will be lifted
     * @param username
     * @return
     */
    @Override
    public String checkBanned(String username) {
        Banned banned = bannedRepository.findOne(username);
        if (banned != null) {
            String time = DateTimeFormat.forPattern(Const.DATE_TIME_PATTERN_BAN).print(DateTime.now(DateTimeZone.forID(Const.TIME_ZONE_ID))
                    .plusSeconds(banned.getBanTime().intValue()));
            return Const.BANNED_MESSAGE + time;
        }
        return null;
    }

    /**
     * check if user is authenticated b4 chack banned status of user
     * @param username
     * @param password
     * @return
     */
    @Override
    public boolean checkValid(String username, String password) {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            if (encoder.matches(password, user.getPassword())) {
                return true;
            }
        }
        return false;
    }


}
