package com.mobile.project.Service.impl;

import com.mobile.project.Constant.Const;
import com.mobile.project.DTO.*;
import com.mobile.project.DTO.Error.FieldErrorDTO;
import com.mobile.project.Enity.Favorite;
import com.mobile.project.Enity.Genre;
import com.mobile.project.Enity.Novel;
import com.mobile.project.Enity.User;
import com.mobile.project.Repository.*;
import com.mobile.project.Service.NovelService;
import com.mobile.project.Utils.Utils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class NovelServiceImpl implements NovelService {

    @Autowired
    ChapterRepository chapterRepository;

    @Autowired
    ReviewRepository reviewRepository;

    @Autowired
    GenreRepository genreRepository;

    @Autowired
    NovelRepostitory novelRepostitory;

    @Autowired
    FavoriteRepository favoriteRepository;

    @Autowired
    UserRepository userRepository;

    /**
     * get list novel for admin
     * @return
     */
    @Override
    public List<AdminNovelDTO> findAllNovelAdmin() {
        List<Novel> novels = novelRepostitory.findAll();
        List<AdminNovelDTO> novelDTOs = new ArrayList<>();
        for (Novel novel : novels) {
            AdminNovelDTO novelDTO = new AdminNovelDTO();
            novelDTO.setActive(novel.isActive());
            novelDTO.setAuthor(novel.getAuthor());
            novelDTO.setNovelId(novel.getNovelId());
            novelDTO.setNovelName(novel.getNovelName());
            if (novel.getYear() == null) {
                novelDTO.setYear(Const.DEFAULT_YEAR);
            } else {
                novelDTO.setYear(novel.getYear());
            }
            int chapterCount = chapterRepository.countByNovelId(novel.getNovelId());
            if (chapterCount > 0) {
                novelDTO.setChapterCount(chapterCount);
            } else novelDTO.setChapterCount(0);
            if (novel.getRatings() != null) {
                novelDTO.setRating(novel.getRatings());
            } else {
                novelDTO.setRating(Const.DEFAULT_RATING);
            }
            novelDTOs.add(novelDTO);
        }
        return novelDTOs;
    }

    /**
     *switch status of novel for admin
     * @param novelid
     */
    @Override
    public void deactivateNovel(int novelid) {
        Novel novel = novelRepostitory.findOne(novelid);
        if (novel != null) {
            boolean status = novel.isActive();
            novel.setActive(!status);
            novelRepostitory.save(novel);
        }
    }

    /**
     * get list of novels for user
     * @param sortBy
     * @return
     */
    @Override
    public List<MiniNovelDTO> findNovelMobile(String sortBy) {
        Sort sort = null;
        if (sortBy.equals(Const.DEFAULT_SORT_NOVELS)) {
            sort = new Sort(Sort.Direction.ASC, sortBy);
        } else {
            sort = new Sort(Sort.Direction.DESC, sortBy);
        }
        List<Novel> novels = novelRepostitory.findByActive(true, sort);
        return listEntityToDTO(novels);
    }

    /**
     * get novel detail for user, also increase the number of views of the novel get viewed
     * @param novelId
     * @return
     */
    @Override
    public NovelDTO viewNovelDetail(int novelId) {
        Novel novel = novelRepostitory.findOne(novelId);
        NovelDTO dto = null;
        if (novel != null) {
            int view = Const.DEFAULT_VIEW;
            if (novel.getViews() != null) {
                view = novel.getViews();
            }
            view++;
            novel.setViews(view);
            novelRepostitory.save(novel);
            dto = enityToDTO(novel);
        }
        return dto;
    }

    /**
     * check if a novel is favorited by user
     * @param novelId
     * @param username
     * @return
     */
    @Override
    public boolean isFavorite(int novelId, String username) {
        Favorite favorite = favoriteRepository.findByNovelIdAndUsername(novelId, username);
        return (favorite != null) ? true : false;
    }

    /**
     * get list genre for admin on add and edit novel page
     * @return
     */
    @Override
    public List<GenreDTO> getListGenre() {
        List<Genre> genres = genreRepository.findAll();
        List<GenreDTO> dtos = new ArrayList<>();
        for (Genre genre : genres) {
            GenreDTO dto = new GenreDTO();
            dto.setGenreId(genre.getGenreId());
            dto.setGenreName(genre.getGenre());
            dtos.add(dto);
        }
        return dtos;
    }

    /**
     * add novel for adminn
     * @param addNovelDTO
     * @return
     */
    @Override
    public boolean addNovel(AddNovelDTO addNovelDTO) {
        if (Utils.isNullOrEmpty(addNovelDTO.getGenreIds()) || Utils.isNullOrEmpty(addNovelDTO.getNovelName()) || Utils.isNullOrEmpty(addNovelDTO.getAuthor())) {
            return false;
        }
        List<Genre> genres = new ArrayList<>();
        Novel novel = new Novel();
        for (Integer genreID : addNovelDTO.getGenreIds()) {
            Genre genre = genreRepository.findOne(genreID);
            genres.add(genre);
        }
        novel.setGenres(genres);
        novel.setNovelName(addNovelDTO.getNovelName());
        novel.setAuthor(addNovelDTO.getAuthor());
        if (addNovelDTO.getImage() == null) {
            novel.setCover(Const.DEFAULT_URL_FOR_COVER);
        } else {
            novel.setCover(addNovelDTO.getImage());
        }
        if (Utils.isNullOrEmpty(addNovelDTO.getDescription())) {
            novel.setDescription(Const.DEFAULT_DESCRIPTION);
        } else {
            novel.setDescription(addNovelDTO.getDescription());
        }
        if (addNovelDTO.getYear() < Const.DEFAULT_YEAR) {
            novel.setYear(Const.DEFAULT_YEAR);
        } else {
            novel.setYear(addNovelDTO.getYear());
        }
        novel.setStatusTranslate(false);
        novel.setActive(true);
        novel.setRatings(Const.DEFAULT_RATING);
        novel.setViews(Const.DEFAULT_VIEW);
        novel.setLastUpdated(new Timestamp(DateTime.now().getMillis()));
        novelRepostitory.save(novel);
        return true;
    }

    /**
     * view novel detail for admin
     * @param novelId
     * @return
     */
    @Override
    public NovelDTO viewNovelDetailAdmin(int novelId) {
        Novel novel = novelRepostitory.findOne(novelId);
        NovelDTO dto = null;
        if (novel != null) {
            dto = enityToDTO(novel);
            if (!Utils.isNullOrEmpty(novel.getCover())) {
                String url = novel.getCover();
                dto.setCover(url);
            } else {
                dto.setCover(Const.DEFAULT_URL_FOR_COVER);
            }
        }
        return dto;
    }

    /**
     * add novel to user's favorite list
     * @param username
     * @param novelId
     * @return
     */
    @Override
    public FieldErrorDTO addFavorite(String username, int novelId) {
        User user = userRepository.findByUsernameAndEnabled(username, true);
        if (user == null) return new FieldErrorDTO(Const.ERROR_USERNAME, Const.ERROR_USERNAME_MESSAGE);
        Novel novel = novelRepostitory.findOne(novelId);
        if (novel == null) return new FieldErrorDTO(Const.ERROR_NOVEL_ID, Const.ERROR_MESSAGE_NOVEL_ID);
        Favorite exist = favoriteRepository.findByNovelIdAndUsername(novelId, username);
        if (exist != null) {
            favoriteRepository.delete(exist);
        } else {
            Favorite favorite = new Favorite();
            favorite.setNovelId(novelId);
            favorite.setUsername(username);
            favoriteRepository.save(favorite);
        }
        return null;
    }

    /**
     * get list of favorite novel for a user
     * @param username
     * @return
     */
    @Override
    public List<MiniNovelDTO> getListFavorites(String username) {
        List<MiniNovelDTO> dtos = new ArrayList<>();
        Sort sort = new Sort(Sort.Direction.ASC, Const.DEFAULT_SORT_NOVELS);
        List<Favorite> favorites = favoriteRepository.findByUsername(username);
        for (Favorite favorite : favorites) {
            Novel novel = novelRepostitory.findByNovelIdAndActive(favorite.getNovelId(), true, sort);
            if (novel != null) {
                MiniNovelDTO dto = new MiniNovelDTO();
                dto.setNovelId(novel.getNovelId());
                if (Utils.isNullOrEmpty(novel.getCover())) {
                    String url = Utils.resizeImage(Const.DEFAULT_URL_FOR_COVER, Const.SUFFIX_SMALL);
                    dto.setCover(url);
                } else {
                    String url = Utils.resizeImage(novel.getCover(), Const.SUFFIX_SMALL);
                    dto.setCover(url);
                }
                dto.setNovelName(novel.getNovelName());
                dto.setAuthor(novel.getAuthor());
                if (novel.getRatings() == null) {
                    dto.setRatings(Const.DEFAULT_RATING);
                } else {
                    dto.setRatings(novel.getRatings());
                }
                dtos.add(dto);
            }
        }
        return dtos;
    }

    /**
     * get list of novels by genre
     * @param genreID
     * @return
     */
    @Override
    public List<MiniNovelDTO> getListNovelByGenre(Integer genreID) {
        Genre genre = genreRepository.findOne(genreID);
        List<MiniNovelDTO> rsList = new ArrayList<>();
        if (genre != null) {
            List<Novel> novels = genre.getNovels();
            if (!Utils.isNullOrEmpty(novels)) {
                novels.sort((Novel n1, Novel n2) -> n1.getNovelName().toLowerCase().compareTo(n2.getNovelName().toLowerCase()));
                rsList = listEntityToDTO(novels);
            }
        }
        return rsList;
    }

    /**
     * get list genre with image is a cover of a random novel long to that genre
     * @return
     */
    @Override
    public List<GenreUserDTO> getListGenreUser() {
        List<Genre> genres = genreRepository.findAll();
        Random r = new Random();
        List<GenreUserDTO> dtos = new ArrayList<>();
        for (Genre genre : genres) {
            GenreUserDTO dto = new GenreUserDTO();
            List<Novel> novels = genre.getNovels();
            String cover = null;
            if (!novels.isEmpty()) {
                cover = novels.get(r.nextInt(novels.size())).getCover();
            }
            if (!Utils.isNullOrEmpty(cover)) {
                dto.setCover(Utils.resizeImage(cover, Const.SUFFIX_SMALL));
            } else {
                dto.setCover(Utils.resizeImage(Const.DEFAULT_URL_FOR_COVER, Const.SUFFIX_SMALL));
            }
            dto.setGenreId(genre.getGenreId());
            dto.setGenreName(genre.getGenre());
            dtos.add(dto);
        }
        return dtos;
    }

    /**
     * search a novel by name, also sort returned list in reuqested order, list only contains 5 novels
     * @param search
     * @param sortBy
     * @return
     */
    @Override
    public List<MiniNovelDTO> searchNovel(String search, String sortBy) {
        Sort sort = null;
        if (sortBy.equals(Const.DEFAULT_SORT_NOVELS)) {
            sort = new Sort(Sort.Direction.ASC, sortBy);
        } else {
            sort = new Sort(Sort.Direction.DESC, sortBy);
        }
        List<Novel> novels = novelRepostitory.findTop5ByActiveAndNovelNameContaining(true, search.toLowerCase(), sort);
        return listEntityToDTO(novels);
    }

    /**
     * edit novel for admin
     * @param novelId
     * @param editNovelDTO
     * @return
     */
    @Override
    public FieldErrorDTO editNovel(Integer novelId, EditNovelDTO editNovelDTO) {
        Novel novel = novelRepostitory.findOne(novelId);
        if(novel == null) {
            return new FieldErrorDTO(Const.ERROR_NOVEL_ID, Const.ERROR_MESSAGE_NOVEL_ID);
        }
        if(editNovelDTO.getStatusTranslated().equals(Const.ONGOING)) {
            novel.setStatusTranslate(false);
        } else {
            novel.setStatusTranslate(true);
        }
        novel.setNovelName(editNovelDTO.getNovelName());
        novel.setAuthor(editNovelDTO.getAuthor());
        novel.setCover(editNovelDTO.getImage());
        if (Utils.isNullOrEmpty(editNovelDTO.getDescription())) {
            novel.setDescription(Const.DEFAULT_DESCRIPTION);
        } else {
            novel.setDescription(editNovelDTO.getDescription());
        }
        if (editNovelDTO.getYear() < Const.DEFAULT_YEAR) {
            novel.setYear(Const.DEFAULT_YEAR);
        } else {
            novel.setYear(editNovelDTO.getYear());
        }
        List<Genre> genres = new ArrayList<>();
        for (Integer genreID : editNovelDTO.getGenreIds()) {
            Genre genre = genreRepository.findOne(genreID);
            genres.add(genre);
        }
        novel.setGenres(genres);
        novel.setLastUpdated(new Timestamp(DateTime.now().getMillis()));
        novelRepostitory.save(novel);
        return null;
    }

    /**
     * turn a list of entity to a list of dto, if cover,rating null then set default value from constant of those field
     * @param novels
     * @return
     */
    private List<MiniNovelDTO> listEntityToDTO(List<Novel> novels) {
        List<MiniNovelDTO> rsList = new ArrayList<>();
        for (Novel novel : novels) {
            if (novel.isActive()) {
                MiniNovelDTO dto = new MiniNovelDTO();
                dto.setNovelId(novel.getNovelId());
                dto.setNovelName(novel.getNovelName());
                if (novel.getCover() == null) {
                    dto.setCover(Utils.resizeImage(Const.DEFAULT_URL_FOR_COVER, Const.SUFFIX_SMALL));
                } else {
                    String url = Utils.resizeImage(novel.getCover(), Const.SUFFIX_SMALL);
                    dto.setCover(url);
                }
                if (!(novel.getRatings() == null)) {
                    dto.setRatings(novel.getRatings());
                } else dto.setRatings(Const.DEFAULT_RATING);
                dto.setAuthor(novel.getAuthor());
                rsList.add(dto);
            }
        }
        return rsList;
    }

    /**
     * turn an entity to dto, if a field is null then set default value from constant for those field
     * @param novel
     * @return
     */
    private NovelDTO enityToDTO(Novel novel) {
        NovelDTO novelDTO = new NovelDTO();
        novelDTO.setNovelId(novel.getNovelId());
        novelDTO.setNovelName(novel.getNovelName());
        novelDTO.setAuthor(novel.getAuthor());
        novelDTO.setActive(novel.isActive());
        if (novel.getStatusTranslate() == null || novel.getStatusTranslate() == false) {
            novelDTO.setStatusTranslate(Const.ONGOING);
        } else {
            novelDTO.setStatusTranslate(Const.COMPLETE);
        }
        if (!Utils.isNullOrEmpty(novel.getCover())) {
            String url = Utils.resizeImage(novel.getCover(), Const.SUFFIX_MEDIUM);
            novelDTO.setCover(url);
        } else {
            novelDTO.setCover(Utils.resizeImage(Const.DEFAULT_URL_FOR_COVER, Const.SUFFIX_MEDIUM));
        }
        novelDTO.setDescription(novel.getDescription());
        if (novel.getLastUpdated() != null) {
            String date = DateTimeFormat.forPattern(Const.DATE_TIME_PATTERN).print(novel.getLastUpdated().getTime());
            novelDTO.setLastUpdated(date);
        } else {
            novelDTO.setLastUpdated(Const.DEFAULT_LAST_UPDATED);
        }
        if (novel.getRatings() != null) {
            novelDTO.setRatings(novel.getRatings());
        } else {
            novelDTO.setRatings(Const.DEFAULT_RATING);
        }
        if (novel.getViews() != null) {
            novelDTO.setViews(novel.getViews());
        } else {
            novelDTO.setViews(Const.DEFAULT_VIEW);
        }
        if (novel.getYear() != null) {
            novelDTO.setYear(novel.getYear());
        } else {
            novelDTO.setYear(Const.DEFAULT_YEAR);
        }
        if (!Utils.isNullOrEmpty(novel.getGenreDTO())) {
            novelDTO.setGenres(novel.getGenreDTO());
        }
        return novelDTO;
    }
}
