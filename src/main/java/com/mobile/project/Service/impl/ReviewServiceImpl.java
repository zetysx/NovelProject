package com.mobile.project.Service.impl;

import com.mobile.project.Constant.Const;
import com.mobile.project.Constant.StatusCode;
import com.mobile.project.DTO.Error.FieldErrorDTO;
import com.mobile.project.DTO.RatingForNovelDetailDTO;
import com.mobile.project.DTO.ReviewAdminDTO;
import com.mobile.project.DTO.ReviewNovelDTO;
import com.mobile.project.DTO.UserReviewDTO;
import com.mobile.project.Enity.Novel;
import com.mobile.project.Enity.Review;
import com.mobile.project.Enity.User;
import com.mobile.project.Repository.NovelRepostitory;
import com.mobile.project.Repository.ReviewRepository;
import com.mobile.project.Repository.UserRepository;
import com.mobile.project.Service.ReviewService;
import com.mobile.project.Utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    ReviewRepository reviewRepository;

    @Autowired
    NovelRepostitory novelRepostitory;

    @Autowired
    UserRepository userRepository;

    /**
     * add new review for user, also update novel rating by average rating of reviews for that novel and update old review if user already review that novel
     * @param userReviewDTO
     * @return
     */
    @Override
    public FieldErrorDTO addNewReview(UserReviewDTO userReviewDTO) {
        User user = userRepository.findByUsernameAndEnabled(userReviewDTO.getUsername(), true);
        if (user == null) return new FieldErrorDTO(Const.ERROR_USERNAME, Const.ERROR_USERNAME_MESSAGE);
        Novel novel = novelRepostitory.findOne(userReviewDTO.getNovelId());
        if (novel == null) return new FieldErrorDTO(Const.ERROR_REVIEW_NOVELID, Const.ERROR_MESSAGE_NOVEL_ID);
        Review addedReview = reviewRepository.findByNovelIdAndUsername(userReviewDTO.getNovelId(), userReviewDTO.getUsername());
        Review review = null;
        if (addedReview != null) {
            review = addedReview;
            review.setRating(userReviewDTO.getRating());
            review.setReviewDetail(userReviewDTO.getReview());
        } else {
            review = dtoToEnity(userReviewDTO);
        }
        reviewRepository.save(review);
        if (novel.getRatings() == null || novel.getRatings() == Const.DEFAULT_RATING) {
            novel.setRatings(userReviewDTO.getRating() * Const.INT_TO_DOUBLE);
        } else {
            List<Integer> ratings = reviewRepository.getRatings(userReviewDTO.getNovelId());
            novel.setRatings(Utils.getAverage(ratings));
        }
        novelRepostitory.save(novel);
        return new FieldErrorDTO(StatusCode.CODE200.getMassage(), Const.REVIEW_ADDEDD_SUCESSFULLY);
    }

    /**
     * check if user already reviewed a novel
     * @param novelId
     * @param username
     * @return
     */
    @Override
    public RatingForNovelDetailDTO isReviewed(int novelId, String username) {
        Review review = reviewRepository.findByNovelIdAndUsername(novelId, username);
        RatingForNovelDetailDTO dto = new RatingForNovelDetailDTO();
        if (review != null) {
            dto.setReview(review.getReviewDetail());
            dto.setRating(review.getRating());
            dto.setReview(true);
        } else {
            dto.setReview(null);
            dto.setReview(false);
            dto.setRating(-1);
        }
        return dto;
    }

    /**
     * get list reviews for a novel for user
     * @param novelId
     * @return
     */
    @Override
    public List<ReviewNovelDTO> getReviewsNovel(int novelId) {
        List<ReviewNovelDTO> reviewDTOs = new ArrayList<>();
        List<Review> reviews = reviewRepository.findByNovelId(novelId);
        for (Review review : reviews) {
            ReviewNovelDTO reviewNovelDTO = new ReviewNovelDTO();
            reviewNovelDTO.setUsername(review.getUsername());
            String avatar = userRepository.findAvatarByUsername(review.getUsername());
            if (Utils.isNullOrEmpty(avatar)) {
                reviewNovelDTO.setAvatar(Utils.resizeImage(Const.DEFAULT_URL_FOR_USER, Const.SUFFIX_TINY));
            } else {
                reviewNovelDTO.setAvatar(Utils.resizeImage(avatar, Const.SUFFIX_TINY));
            }
            reviewNovelDTO.setRating(review.getRating());
            reviewNovelDTO.setReview(review.getReviewDetail());
            reviewDTOs.add(reviewNovelDTO);
        }
        return reviewDTOs;
    }

    /**
     * delete review for user, update novel rating afterward
     * @param novelId
     * @param username
     * @return
     */
    @Override
    public FieldErrorDTO deleteReview(int novelId, String username) {
        User user = userRepository.findByUsernameAndEnabled(username, true);
        if (user == null) return new FieldErrorDTO(Const.ERROR_USERNAME, Const.ERROR_USERNAME_MESSAGE);
        Novel novel = novelRepostitory.findOne(novelId);
        if (novel == null) return new FieldErrorDTO(Const.ERROR_REVIEW_NOVELID, Const.ERROR_MESSAGE_NOVEL_ID);
        Review deleted = reviewRepository.findByNovelIdAndUsername(novelId, username);
        if (deleted != null) {
            reviewRepository.delete(deleted);
            List<Integer> ratings = reviewRepository.getRatings(novelId);
            novel.setRatings(Utils.getAverage(ratings));
            novelRepostitory.save(novel);
        } else {
            return new FieldErrorDTO(Const.ERROR_REVIEW_NOVELID, Const.ERROR_REVIEW);
        }
        return null;
    }

    /**
     * get list review for admin
     * @return
     */
    @Override
    public List<ReviewAdminDTO> getListReivews() {
        List<ReviewAdminDTO> dtos = new ArrayList<>();
        List<Review> reviews = reviewRepository.findAll();
        for (Review review : reviews) {
            ReviewAdminDTO dto = new ReviewAdminDTO();
            Novel novel = novelRepostitory.findOne(review.getNovelId());
            dto.setReviewId(review.getReviewId());
            dto.setUsername(review.getUsername());
            dto.setReview(review.getReviewDetail());
            dto.setRating(review.getRating());
            dto.setNovelName(novel.getNovelName());
            dtos.add(dto);
        }
        return dtos;
    }

    /**
     * delete review for admin, update novel rating afterward
     * @param reviewId
     * @return
     */
    @Override
    public FieldErrorDTO deleteReviewForAdmin(Integer reviewId) {
        Review deleted = reviewRepository.findOne(reviewId);
        if (deleted != null) {
            reviewRepository.delete(deleted);
            List<Integer> ratings = reviewRepository.getRatings(deleted.getNovelId());
            Novel novel = novelRepostitory.findOne(deleted.getNovelId());
            novel.setRatings(Utils.getAverage(ratings));
            novelRepostitory.save(novel);
        } else {
            return new FieldErrorDTO(Const.ERROR_REVIEW_NOVELID, Const.ERROR_REVIEW_ADMIN);
        }
        return null;
    }

    /**
     * turn dto to entity
     * @param userReviewDTO
     * @return
     */
    private Review dtoToEnity(UserReviewDTO userReviewDTO) {
        Review review = new Review();
        review.setNovelId(userReviewDTO.getNovelId());
        review.setUsername(userReviewDTO.getUsername());
        review.setRating(userReviewDTO.getRating());
        review.setReviewDetail(userReviewDTO.getReview());
        return review;
    }
}
