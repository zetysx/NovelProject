package com.mobile.project.Service.impl;

import com.mobile.project.Constant.Const;
import com.mobile.project.DTO.AccountDTO;
import com.mobile.project.DTO.AccountForAdminDTO;
import com.mobile.project.DTO.EditAccountDTO;
import com.mobile.project.DTO.Error.FieldErrorDTO;
import com.mobile.project.DTO.UserDetailDTO;
import com.mobile.project.Enity.Banned;
import com.mobile.project.Enity.User;
import com.mobile.project.Repository.BannedRepository;
import com.mobile.project.Repository.UserRepository;
import com.mobile.project.Service.AccountService;
import com.mobile.project.Utils.Utils;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    BannedRepository bannedRepository;

    /**
     *  register user account
      * @param accountDTO
     * @return
     */
    @Override
    public FieldErrorDTO register(AccountDTO accountDTO) {
        User duplicate = userRepository.findOne(accountDTO.getUsername());
        if (duplicate != null) {
            return new FieldErrorDTO(Const.ERROR_USERNAME, Const.ERROR_USERNAME_DUPLICATE);
        }
        User user = new User();
        user.setUsername(accountDTO.getUsername());
        user.setPassword(encoder.encode(accountDTO.getPassword()));
        user.setEnabled(true);
        user.setRole(Const.ROLE_USER);
        if (Utils.isNullOrEmpty(accountDTO.getAvatar())) {
            user.setAvatar(Const.DEFAULT_URL_FOR_USER);
        } else {
            user.setAvatar(accountDTO.getAvatar());
        }
        SimpleDateFormat sdf = new SimpleDateFormat(Const.DATE_PATTERN);
        Date date = null;
        if (!Utils.isNullOrEmpty(accountDTO.getBirthday())) {
            try {
                date = sdf.parse(accountDTO.getBirthday());
            } catch (ParseException e) {
                return new FieldErrorDTO(Const.ERROR_BIRTHDATE, Const.ERROR_BIRTHDATE_MESSAGE);
            }
        }
        if (date != null) {
            user.setBirthday(new java.sql.Date(date.getTime()));
        }
        user.setFullname(accountDTO.getFullname());
        user.setSex(accountDTO.getSex());
        userRepository.save(user);
        return null;
    }

    /**
     *
     * get user detail
     * @param username
     * @return
     */
    @Override
    public UserDetailDTO getUserDetail(String username) {
        User user = userRepository.findOne(username);
        if (user != null) {
            DateTimeFormatter dtf = DateTimeFormat.forPattern(Const.DATE_PATTERN);
            UserDetailDTO dto = new UserDetailDTO();
            dto.setUsername(user.getUsername());
            if (user.getBirthday() != null) {
                dto.setBirthday(dtf.print(user.getBirthday().getTime()));
            }
            dto.setFullname(user.getFullname());
            dto.setGender(user.getSex());
            if (Utils.isNullOrEmpty(user.getAvatar())) {
                dto.setAvatar(Const.DEFAULT_URL_FOR_USER);
            } else {
                dto.setAvatar(user.getAvatar());
            }
            return dto;
        }
        return null;
    }

    /**
     * edit user detail
     * @param username
     * @param accountDTO
     * @return
     */
    @Override
    public FieldErrorDTO editUser(String username, EditAccountDTO accountDTO) {
        User user = userRepository.findOne(username);
        if (user == null) {
            return new FieldErrorDTO(Const.ERROR_USERNAME, Const.ERROR_USERNAME_MESSAGE);
        }
        if (accountDTO.getPassword() != null) {
            String password = encoder.encode(accountDTO.getPassword());
            user.setPassword(password);
        }
        if (!Utils.isNullOrEmpty(accountDTO.getAvatar())) {
            user.setAvatar(accountDTO.getAvatar());
        } else {
            user.setAvatar(Const.DEFAULT_URL_FOR_USER);
        }
        SimpleDateFormat sdf = new SimpleDateFormat(Const.DATE_PATTERN);
        Date date = null;
        if (!Utils.isNullOrEmpty(accountDTO.getBirthday())) {
            try {
                date = sdf.parse(accountDTO.getBirthday());
                user.setBirthday(new java.sql.Date(date.getTime()));
            } catch (Exception e) {
                return new FieldErrorDTO(Const.ERROR_BIRTHDATE, Const.ERROR_BIRTHDATE_MESSAGE);
            }
        } else {
            user.setBirthday(null);
        }
        user.setFullname(accountDTO.getFullname());
        user.setSex(accountDTO.getSex());
        userRepository.save(user);
        return null;
    }

    /**
     * get list user for admin
     * @return
     */
    @Override
    public List<AccountForAdminDTO> getListUsers() {
        List<User> users = userRepository.findAllByRole(Const.ROLE_USER);
        List<AccountForAdminDTO> dtos = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat(Const.DATE_PATTERN);
        for (User user : users) {
            AccountForAdminDTO dto = new AccountForAdminDTO();
            dto.setUsername(user.getUsername());
            if (Utils.isNullOrEmpty(user.getAvatar())) {
                dto.setAvatar(Const.DEFAULT_URL_FOR_USER);
            } else {
                dto.setAvatar(user.getAvatar());
            }
            String date = null;
            if (user.getBirthday() != null) {
                date = sdf.format(user.getBirthday());
            }
            dto.setBirthday(date);
            dto.setFullname(user.getFullname());
            dto.setSex(user.getSex());
            dto.setEnable(user.isEnabled());
            dtos.add(dto);
        }
        return dtos;
    }

    /**
     * Ban/unban a user account
     * @param username
     * @param banTime
     * @return
     */
    @Override
    public FieldErrorDTO deactivateUser(String username, Integer banTime) {
        User user = userRepository.findOne(username);
        if (user == null) {
            return new FieldErrorDTO(Const.ERROR_USERNAME, Const.ERROR_USERNAME_MESSAGE);
        }
        if (banTime == null) {
            if (!user.isEnabled()) {
                Banned banned = bannedRepository.findOne(username);
                if (banned != null) {
                    bannedRepository.delete(banned);
                }
                user.setEnabled(true);
            } else {
                return new FieldErrorDTO(Const.ERROR_USERNAME, Const.BAN_TIME);
            }
        } else {
            Banned banned = bannedRepository.findOne(username);
            if (banned == null) {
                banned = new Banned();
                banned.setUsername(username);
            }
            banned.setBanTime(banTime.longValue());
            bannedRepository.save(banned);
            user.setEnabled(false);
        }
//        if (user.isEnabled()) {
//            user.setEnabled(false);
//        } else {
//            user.setEnabled(true);
//        }
        userRepository.save(user);
        return null;
    }

    /**
     * Change password for admin
     * @param username
     * @param password
     * @return
     */
    @Override
    public boolean changePassword(String username, String password) {
        User user = userRepository.findOne(username);
        user.setPassword(encoder.encode(password));
        userRepository.save(user);
        return true;
    }
}
