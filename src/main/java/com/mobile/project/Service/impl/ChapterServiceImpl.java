package com.mobile.project.Service.impl;

import com.mobile.project.Constant.Const;
import com.mobile.project.DTO.*;
import com.mobile.project.DTO.Error.FieldErrorDTO;
import com.mobile.project.Enity.Chapter;
import com.mobile.project.Enity.Novel;
import com.mobile.project.Repository.ChapterRepository;
import com.mobile.project.Repository.NovelRepostitory;
import com.mobile.project.Service.ChapterService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class ChapterServiceImpl implements ChapterService {

    @Autowired
    ChapterRepository chapterRepository;

    @Autowired
    NovelRepostitory novelRepostitory;

    public static final SimpleDateFormat sdf = new SimpleDateFormat(Const.DATE_JSON_PATTERN);

    /**
     * get list chapter for user
     * @param novelId
     * @return
     */
    @Override
    public List<MiniChapterDTO> getListChapters(int novelId) {
        List<MiniChapterDTO> dtos = chapterRepository.getListChapters(novelId);
        return dtos;
    }

    /**
     * get 1 chapter for user
     * @param chapterID
     * @return
     */
    @Override
    public ChapterDTO getOneChapter(int chapterID) {
        Chapter chapter = chapterRepository.findOne(chapterID);
        SimpleDateFormat sdf = new SimpleDateFormat(Const.DATE_PATTERN);
        ChapterDTO dto = null;
        if (chapter != null) {
            dto = new ChapterDTO();
            dto.setChapterContent(chapter.getChapterContent());
            dto.setChapterId(chapter.getChapterId());
            dto.setChapterTitle(chapter.getChapterTitle());
            dto.setNovelId(chapter.getNovelId());
            if (chapter.getReleaseDate() != null) {
                String date = sdf.format(chapter.getReleaseDate());
                dto.setReleaseDate(date);
            }
        }
        return dto;
    }

    /**
     * add chapter for admin, update last updated of corresponding novel
     * @param addChapterDTO
     * @return
     */
    @Override
    public FieldErrorDTO addChapter(AddChapterDTO addChapterDTO) {
        Novel novel = novelRepostitory.findOne(addChapterDTO.getNovelId());
        if (novel == null) {
            return new FieldErrorDTO(Const.ERROR_NOVEL_ID, Const.ERROR_MESSAGE_NOVEL_ID);
        }
        Timestamp now = new Timestamp(DateTime.now().getMillis());
        novel.setLastUpdated(now);
        Chapter chapter = new Chapter();
        chapter.setNovelId(addChapterDTO.getNovelId());
        chapter.setChapterTitle(addChapterDTO.getTitle());
        chapter.setChapterContent(addChapterDTO.getContent());
        chapter.setReleaseDate(now);
        chapterRepository.save(chapter);
        return null;
    }

    /**
     * get list chapters for admin
     * @param novelId
     * @return
     */
    @Override
    public AdminChapterListDTO getListChaptersAdmin(int novelId) {
        AdminChapterListDTO dto = new AdminChapterListDTO();
        Novel novel = novelRepostitory.findOne(novelId);
        if (novel == null) {
            return null;
        }
        List<ChapterDetailDTO> chapters = chapterRepository.getListChapterAdmin(novelId);
        dto.setCover(novel.getCover());
        dto.setNovemName(novel.getNovelName());
        dto.setAuthor(novel.getAuthor());
        dto.setChapters(chapters);
        return dto;
    }

    /**
     * edit chapter for admin
     * @param chapterId
     * @param editChapterDTO
     * @return
     */
    @Override
    public FieldErrorDTO editChapter(Integer chapterId, EditChapterDTO editChapterDTO) {
        Chapter chapter = chapterRepository.findOne(chapterId);
        if (chapter == null) {
            return new FieldErrorDTO(Const.ERROR_CHAPTER_ID, Const.ERROR_MESSAGE_CHAPTER_ID);
        }
        chapter.setChapterTitle(editChapterDTO.getTitle());
        chapter.setChapterContent(editChapterDTO.getContent());
        chapterRepository.save(chapter);
        Novel novel = novelRepostitory.findOne(chapter.getNovelId());
        novel.setLastUpdated(new Timestamp(DateTime.now().getMillis()));
        novelRepostitory.save(novel);
        return null;
    }
}
