package com.mobile.project.Service;

import com.mobile.project.DTO.*;
import com.mobile.project.DTO.Error.FieldErrorDTO;

import java.util.List;

public interface NovelService {
    public List<AdminNovelDTO> findAllNovelAdmin();

    public void deactivateNovel(int novelid);

    public List<MiniNovelDTO> findNovelMobile(String sortBy);

    public NovelDTO viewNovelDetail(int novelId);

    public boolean isFavorite(int novelId, String username);

    public List<GenreDTO> getListGenre();

    public boolean addNovel(AddNovelDTO addNovelDTO);

    public NovelDTO viewNovelDetailAdmin(int novelId);

    public FieldErrorDTO addFavorite(String username, int novelId);

    public List<MiniNovelDTO> getListFavorites(String username);

    public List<MiniNovelDTO> getListNovelByGenre(Integer genreID);

    public List<GenreUserDTO> getListGenreUser();

    public List<MiniNovelDTO> searchNovel(String search, String sortBy);

    public FieldErrorDTO editNovel(Integer novelId, EditNovelDTO editNovelDTO);
}
