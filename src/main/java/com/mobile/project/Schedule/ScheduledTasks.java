package com.mobile.project.Schedule;

import com.mobile.project.Enity.Banned;
import com.mobile.project.Enity.User;
import com.mobile.project.Repository.BannedRepository;
import com.mobile.project.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ScheduledTasks {

    @Autowired
    BannedRepository bannedRepository;

    @Autowired
    UserRepository userRepository;

    @Value("${ban.time}")
    private int banReduce;

    /**
     * scheduling for reduce ban time every amount of time conigured in application.yml, if ban time enable the user and remove the user from ban
     */
    @Scheduled(cron = "${ban.cron}")
    public void liftBan() {
        List<Banned> bannedes = bannedRepository.findAll();
        for (Banned banned : bannedes) {
            Long banTime = banned.getBanTime();
            if (banTime >= banReduce) {
                banTime -= banReduce;
                banned.setBanTime(banTime);
                bannedRepository.save(banned);
            } else {
                bannedRepository.delete(banned);
                User user = userRepository.findOne(banned.getUsername());
                user.setEnabled(true);
                userRepository.save(user);
            }
        }
    }
}
