package com.mobile.project.Config;

import com.mobile.project.Constant.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableAutoConfiguration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Qualifier("dataSource")
    @Autowired
    DataSource dataSource;

    @Autowired
    PasswordEncoder passwordEncoder;


    //authenticate and authorize user
    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery(Const.SQL_QUERY_USERNAME_PASSWORD)
                .passwordEncoder(passwordEncoder)
                .authoritiesByUsernameQuery(Const.SQL_QUERY_USERNAME_ROLE);
    }

    //handle and forward request
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(Const.ROOT_API_FOR_USER + Const.MATCHER, Const.ROOT_AUTHORIZATION + Const.MATCHER).permitAll()
                .antMatchers(Const.MATCHER, Const.ROOT_API_FOR_ADMIN + Const.MATCHER)
//                .permitAll()
                .hasRole(Const.ADMIN)
                .anyRequest().authenticated().and().formLogin().loginPage(Const.ADMIN_LOGIN).permitAll()
                .defaultSuccessUrl(Const.ADMIN_LIST_NOVEL)
                .failureUrl(Const.LOGIN_FAILURE_URL)
                .and().logout().clearAuthentication(true).logoutUrl(Const.LOGOUT)
                .permitAll().logoutSuccessUrl(Const.ADMIN_LOGIN)
                .and().exceptionHandling().accessDeniedPage(Const.ACCESS_DENIED)
                .and().csrf().disable();
    }

    //bean for password encrypter
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
